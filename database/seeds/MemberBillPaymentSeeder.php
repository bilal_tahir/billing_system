<?php

use Illuminate\Database\Seeder;

class MemberBillPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('member_bill_payments')->insert([
            'membership_no' => '0001',
           'month'=> 'September',
           'year' => '2019',
           'amount' => 3450, 
       ]);
       DB::table('member_bill_payments')->insert([
        'membership_no' => '0001',
       'month'=> 'October',
       'year' => '2019',
       'amount' => 3450, 
   ]);
   DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'November',
   'year' => '2019',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'December',
   'year' => '2019',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'January',
   'year' => '2020',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'March',
   'year' => '2020',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'May',
   'year' => '2020',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0001',
   'month'=> 'June',
   'year' => '2020',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0006',
   'month'=> 'April',
   'year' => '2020',
   'amount' => 3450, 
]);
DB::table('member_bill_payments')->insert([
    'membership_no' => '0006',
   'month'=> 'February',
   'year' => '2020',
   'amount' => 3450, 
]);


DB::table('member_bill_payments')->insert([
    'membership_no' => '7666',
   'month'=> 'August',
   'year' => '2019',
   'amount' => 3450, 
]);


DB::table('member_bill_payments')->insert([
    'membership_no' => '0006',
   'month'=> 'July',
   'year' => '2019',
   'amount' => 3450, 
]);


    }
}
