<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(WaterSeeder::class);
        // $this->call(ConservanciesSeeder::class);
        $this->call(SurchargeSeeder::class);
        $this->call(UserRecordSeeder::class);
        // $this->call(StnSeeder::class);
        // $this->call(LogoSeeder::class);
        $this->call(BillSeeder::class);
        // $this->call(MemberBillPaymentSeeder::class);

    }
}
