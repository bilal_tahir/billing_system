<?php

use Illuminate\Database\Seeder;

class ConservanciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('conservancies')->insert([
             'size' => 1000,
            'sl_charge' => 104,
            'security_charge' => 104,
            'rmgc_charge' => 104,
            'gst' => 104,  
        ]);
         DB::table('conservancies')->insert([
             'size' => 1250,
            'sl_charge' => 198,
            'security_charge' => 198,
            'rmgc_charge' => 198,
            'gst' => 198,  
        ]);
         DB::table('conservancies')->insert([
             'size' => 1800,
            'sl_charge' => 214,
            'security_charge' => 214,
            'rmgc_charge' => 214,
            'gst' => 214,  
        ]);
         DB::table('conservancies')->insert([
             'size' => 2450,
            'sl_charge' => 244,
            'security_charge' => 244,
            'rmgc_charge' => 244,
            'gst' => 244,  
        ]);
         DB::table('conservancies')->insert([
             'size' => 4500,
            'sl_charge' => 290,
            'security_charge' => 290,
            'rmgc_charge' => 290,
            'gst' => 290,  
        ]);
     
    }
}
