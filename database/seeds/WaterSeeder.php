<?php

use Illuminate\Database\Seeder;

class WaterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('waters')->insert([
           'size' => 1000,
            'monthly_charges' => 180,  
        ]);
        DB::table('waters')->insert([
           'size' => 1250,
            'monthly_charges' => 180,  
        ]);
        DB::table('waters')->insert([
           'size' => 1800,
            'monthly_charges' => 180,  
        ]);
        DB::table('waters')->insert([
           'size' => 2450,
            'monthly_charges' => 180,  
        ]);
        DB::table('waters')->insert([
           'size' => 4500,
            'monthly_charges' => 180,  
        ]);
    }
}
