<?php

use Illuminate\Database\Seeder;

class SurchargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('surcharges')->insert([
            'size' => 1200,
            'surcharge' => 104,  
        ]);
        DB::table('surcharges')->insert([
            'size' => 1500,
            'surcharge' => 123,  
        ]);
        DB::table('surcharges')->insert([
            'size' => 1800,
            'surcharge' => 130,  
        ]);
        DB::table('surcharges')->insert([
            'size' => 2000,
            'surcharge' => 150,  
        ]);
        DB::table('surcharges')->insert([
            'size' =>2700,
            'surcharge' => 165,  
        ]);
        DB::table('surcharges')->insert([
            'size' =>500,
            'surcharge' => 15,  
        ]);
    }
}
