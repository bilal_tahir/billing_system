<?php

use Illuminate\Database\Seeder;

class StnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stns')->insert([
           'ntn' => '0-012345678',
            'strn' => '1234567-98',  
        ]);
        
        DB::table('users')->insert([
           'name' => 'System Admin',
            'email' => 'admin@admin.com',  
            'password' => bcrypt('system@admin9900;'),
        ]);
    }
}
