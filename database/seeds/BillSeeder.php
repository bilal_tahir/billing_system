<?php

use Illuminate\Database\Seeder;

class BillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('bills')->insert([
             'marla' => '4 Marla',
            'size' => 1200,
            'charges' => 3450,   
        ]);
          DB::table('bills')->insert([
             'marla' => '5 Marla',
            'size' => 1500,
            'charges' => 4130,   
        ]);
          DB::table('bills')->insert([
             'marla' => '7 Marla',
            'size' => 1800,
            'charges' => 4680,   
        ]);
          DB::table('bills')->insert([
             'marla' => '10 Marla',
            'size' => 2000,
            'charges' => 4990,   
        ]);
          DB::table('bills')->insert([
             'marla' => '1 Kanal',
            'size' => 2700,
            'charges' => 5400,   
        ]);
           
        DB::table('bills')->insert([
            'marla' => 'Commercial Unit, Shop/ Office/ Flat etc.',
           'size' => 500,
           'charges' => 760,   
       ]);
          
        DB::table('users')->insert([
           'name' => 'System Admin',
            'email' => 'admin@admin.com',  
            'password' => bcrypt('system@admin9900;'),
        ]);
    }
}
