<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConservanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conservancies', function (Blueprint $table) {
            $table->id();
            $table->integer('size');
            $table->integer('sl_charge');
            $table->integer('security_charge');
            $table->integer('rmgc_charge');
            $table->integer('gst');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conservancies');
    }
}
