<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_records', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('membership_no')->unique();
            $table->string('sdo');
            $table->date('booking_date');
            $table->string('plot_no')->unique();
            $table->string('street_no');
            $table->integer('size');
            $table->string('block');
            $table->string('cnic');
            $table->string('contact1');
            $table->string('contact2')->nullable();
            $table->string('address');
            $table->string('email')->nullable();
            $table->string('paymentmode')->nullable();
            $table->string('mutation');
            $table->string('possession');
            // $table->integer('no_of_connections')->default(0)->nullable();
            $table->integer('arrears')->default(0);
            $table->string('billing_month')->nullable();
            $table->string('billing_year')->nullable();
            $table->string('status');
             $table->integer('plotprice')->nullable();
            $table->integer('payment_due')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_records');
    }
}
