<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allotes', function (Blueprint $table) {
            $table->id();
            $table->string('registerationNo');
            $table->string('bookingDate');
            $table->string('plotType');
            $table->string('plotSize');
            $table->string('plotNo');
            $table->string('streetNo');
            $table->string('block');
            $table->string('name');
            $table->string('fName');
            $table->string('nicNo');
            $table->string('phoneNo');
            $table->string('postalAddress');
            $table->string('permanentAddress');
            $table->string('nominationName');
            $table->string('nominationfName');
            $table->string('nominationAddress');
            $table->string('paymentMode');
            $table->string('paymentType');
            $table->integer('paymentNo');
            $table->string('receiptNo')->nullable();
            $table->integer('amount')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allotes');
    }
}
