<?php

namespace App\Http\Controllers;

use App\Allote;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AlloteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Allote  $allote
     * @return \Illuminate\Http\Response
     */
    public function show(Allote $allote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Allote  $allote
     * @return \Illuminate\Http\Response
     */
    public function edit(Allote $allote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Allote  $allote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Allote $allote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Allote  $allote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Allote $allote)
    {
        //
    }
    public function regformview()
    {
        return view ('allotee.form');
    }
    public function regallote()
    {
        return view ('allotee.register');
    }

    public function allotereg(Request $request)
    {

        $files = $request->validate([
            'name' =>
            array(
                'required',
                'regex:/^[a-zA-Z ]*$/'

                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),
            'registerationNo' => 'required|min:3',
            'fName' =>   array(
                'required',
                'regex:/^[a-zA-Z ]*$/'

                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),

            'nominationName' =>
            array(
                'required',
                'regex:/^[a-zA-Z ]*$/'

                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),
            'nominationfName' =>   array(
                'required',
                'regex:/^[a-zA-Z ]*$/'

                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),

            'plotNo' => 'required|integer',
            // 'block' => 'required',
            'postalAddress' => 'required',
            'permanentAddress' => 'required',
            'streetNo' => 'required|integer',

            'nominationAddress' => 'required',


            'block' =>    array(
                'required',
                'regex:/^[A-Z ]*$/',
                'max:1',

                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),
            'nicNo' =>  array(
                'required',


                // if (!preg_match("/^[a-zA-Z ]*$/",$name))


            ),
            'phoneNo' => array(
                'required',
            ),



            'no' => 'required|integer',

            // 'paymentmode' => 'required',
            'amount' => 'required|integer',

            // 'plotprice' => 'required|numeric',
            // 'connection' => 'required|numeric',



]);

// dd($request);

        $alote = new Allote ();
        $alote->registerationNo = $request->registerationNo ;
        $alote->bookingDate = Carbon::now()->format('d-m-Y'); ;
        $alote->plotType = $request->plotType ;
        $alote->plotSize = $request->plotSize ;
        $alote->PlotNo = $request->plotNo ;
        $alote->streetNo = $request->streetNo ;
        $alote->block = $request->block ;
        $alote->name = $request->name ;
        $alote->fname = $request->fName ;
        $alote->nicNo = $request->nicNo ;
        $alote->phoneNo = $request->phoneNo ;
        $alote->postalAddress = $request->postalAddress ;
        $alote->permanentAddress = $request->permanentAddress ;
        $alote->nominationName = $request->nominationName ;
        $alote->nominationfName = $request->nominationfName ;
        $alote->nominationAddress = $request->nominationAddress ;
        $alote->paymentMode = $request->paymentMode ;
        $alote->paymentType = $request->paymentFrom ;
        $alote->paymentNo = $request->no ;
        $alote->receiptNo = $request->receiptNo ;
        $alote->amount = $request->amount ;
        $alote->remarks = $request->remarks ;
        if($alote->save())
        {
            return redirect()->back()->withSuccess('Great! Registered successfully.');
        }
        else{
            return redirect()->back()->withSuccess('Failed! Not Registered successfully.');
        }
    }




    public function printalloteform(Request $request)
    {
        $alote = Allote::where('registerationNo',$request->registerationNo)->first();
        return view ('allotee.form')->with([
            'alote' => $alote,
        ]);
        // dd($request->registerationNo);
    }
}
