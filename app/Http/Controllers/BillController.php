<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Surcharge;
use Illuminate\Http\Request;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $bill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function edit(Bill $bill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bill $bill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bill $bill)
    {
        //
    }

    public function updatebill($id)
    {
;
        $sur = Bill::where('id',$id)->first();
        return view ('memberwebsite.updatebill')->with([
            'sur' => $sur,
          
        ]);
    }

    public function deletebill($id)
    {
$d = Bill::where('id',$id)->delete();
        return redirect()->back()->withSuccess('Great! Water Deleted successfully.');
    }
    public function billupdate(Request $request)
    {
        $files = $request->validate([
               
                'size' => 'required|numeric',
             
                'charges' => 'required|numeric',
               

                
    ]);

     $c = Bill::where('id',$request->id)->update([
                    'size' => $request->size,
                    'charges' => $request->charges,
                    

                ]);

                return redirect()->back()->withSuccess('Great! Bill edited successfully.');

    }

    public function add(Request $request)
    {
        $files = $request->validate([
               
            'size' => 'required|numeric',
            'marla' => 'required',
         
            'charges' => 'required|numeric',
           

            
]);
        $bill = New Bill();
        $bill->marla = $request->marla;
        $bill->size = $request->size;
        $bill->charges = $request->charges;

        $bill->save();
        return redirect()->back();

    }

    public function updatecon()
    {
        $b = Bill::all();
        $ar = Surcharge::all();
        return view ('memberwebsite.update')->with([
            'b' => $b,
            'ar' => $ar,
        ]);
    }
}
