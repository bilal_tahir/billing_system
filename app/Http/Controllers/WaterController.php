<?php

namespace App\Http\Controllers;

use App\Water;
use App\Logo;
use Illuminate\Http\Request;

class WaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function show(Water $water)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function edit(Water $water)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Water $water)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Water  $water
     * @return \Illuminate\Http\Response
     */
    public function destroy(Water $water)
    {
        //
    }

    
    public function editwater($id)
    {
        $water = Water::where('id',$id)->first();
        return view ('memberwebsite.updatewater')->with([
            'water' => $water,
            'logo' => $logo,
        ]);
    }
    public function wateredit(Request $request)
    {
         $files = $request->validate([
               
                'size' => 'required|numeric',
             
                'monthly' => 'required|numeric',
               

                
    ]);

     $c = Water::where('id',$request->id)->update([
                    'size' => $request->size,
                    'monthly_charges' => $request->monthly,
                    

                ]);

                return redirect()->back()->withSuccess('Great! Conservancies edited successfully.');
    }

    public function deletewater($id)
    {
          $d = Water::where('id',$id)->delete();
        return redirect()->back()->withSuccess('Great! Water Deleted successfully.');
    }

    public function addwater(Request $request)
    {

            $files = $request->validate([ 
                'size' => 'required|numeric',
                'monthly' => 'required|numeric',
    ]);
        $wat = new Water();
        $wat->size = $request->size;
        $wat->monthly_charges = $request->monthly;

            if ($wat->save())
{
    return redirect()->back()->withSuccess('Great! Water Bil Added successfully.');
}

    }
}
