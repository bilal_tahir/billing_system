<?php

namespace App\Http\Controllers;

use App\Surcharge;
use App\Logo;
use App\Bill;
use Illuminate\Http\Request;

class SurchargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Surcharge  $surcharge
     * @return \Illuminate\Http\Response
     */
    public function show(Surcharge $surcharge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Surcharge  $surcharge
     * @return \Illuminate\Http\Response
     */
    public function edit(Surcharge $surcharge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Surcharge  $surcharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Surcharge $surcharge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Surcharge  $surcharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Surcharge $surcharge)
    {
        //
    }
    public function editsurcharge($id)
    {
        $logo = Logo::where('id',1)->first();
        $sur = Surcharge::where('id',$id)->first();
        return view ('memberwebsite.updatesurcharge')->with([
            'sur' => $sur,
            'logo' => $logo,
        ]);
    }


    public function editsur(Request $request)
    {
        $files = $request->validate([
               
                'size' => 'required|numeric',
             
                'surcharge' => 'required|numeric',
               

                
    ]);

     $c = Surcharge::where('id',$request->id)->update([
                    'size' => $request->size,
                    'surcharge' => $request->surcharge,
                    

                ]);

                return redirect()->back()->withSuccess('Great! Surcharge edited successfully.');

    }

    public function deletesurcharge($id)
    {
        $d = Surcharge::where('id',$id)->delete();
        return redirect()->back()->withSuccess('Great! Water Deleted successfully.');
    }

    public function addsurcharge(Request $request)
    {
           $files = $request->validate([
               
                'size' => 'required|numeric',
                'surcharge' => 'required|numeric',
    ]);
    

    $rt = Bill::where('size',$request->size)->first();
    if($rt)
    {

    $sur = new Surcharge();
    $sur->size = $request->size;
    $sur->surcharge = $request->surcharge;

        if ($sur->save())
{
    return redirect()->back();
}
    }
    else
    {
        return redirect()->back();
    }
    }
}
