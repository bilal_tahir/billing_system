<?php

namespace App\Http\Controllers;

use App\PaymentRecord;
use App\UserRecord;
use App\Record;
use App\MemberBillPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentRecord  $paymentRecord
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentRecord $paymentRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentRecord  $paymentRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentRecord $paymentRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentRecord  $paymentRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentRecord $paymentRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentRecord  $paymentRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentRecord $paymentRecord)
    {
        //
    }

    public function adduserbill($id)
    {

        $userrec = UserRecord::where('id',$id)->first();
if($userrec->membership_no != 0 ){


    $rec = new Record();
    $rec->membership = $userrec->membership_no;
    $rec->month = $userrec->billing_month;
    $rec->year = $userrec->billing_year;
    $rec->payment = $userrec->arrears;
    $rec->bill = $userrec->arrears;
    $rec->save();

        $pay = new PaymentRecord();
        $pay->registeration_no = $userrec->membership_no;
        $pay->month = Carbon::now()->format('F');
        $pay->year = Carbon::now()->format('Y');
        $pay->payment = $userrec->arrears;

        if($pay->save())
        {

    $bil = new MemberBillPayment();
    $bil->membership_no = $userrec->membership_no;
    $bil->month = Carbon::now()->subMonth(0)->format('F');
    $bil->year = Carbon::now()->subMonth(0)->format('Y');

    $bil->amount = $userrec->arrears;
    $bil->save();


    $trr = Record::where('membership',$userrec->membership_no)->latest()->update([
        'payment' => $userrec->arrears,
    ]);


     $bill = UserRecord::where('id',$id)->update([
        'arrears' => 0,
    ]);







return redirect()->back();
        }

    }
    else{
        return redirect()->back();
    }

    }

    public function undouserbill($id)
    {
        $ud = UserRecord::where('id',$id)->first();
        $rr = PaymentRecord::where('registeration_no',$ud->membership_no)->latest()->first();
        if($rr)
        {
        $usss = UserRecord::where('membership_no',$rr->registeration_no)->update([
            'arrears' => $rr->payment,
        ]);

        $dr = Record::where('membership',$rr->registeration_no)->latest()->update([
            'payment' => 0,
        ]);

        $dm = MemberBillPayment::where('membership_no',$rr->registeration_no)->latest()->delete();

        $dp = PaymentRecord::where('registeration_no',$rr->registeration_no)->latest()->delete();

        Record::where('membership',$rr->registeration_no)->latest()->delete();
        return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }

    }
}
