<?php

namespace App\Http\Controllers;

use App\Conservancies;
use App\Logo;
use Illuminate\Http\Request;

class ConservanciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conservancies  $conservancies
     * @return \Illuminate\Http\Response
     */
    public function show(Conservancies $conservancies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conservancies  $conservancies
     * @return \Illuminate\Http\Response
     */
    public function edit(Conservancies $conservancies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conservancies  $conservancies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conservancies $conservancies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conservancies  $conservancies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conservancies $conservancies)
    {
        //
    }

    public function editbill($id)
    {
        $logo = Logo::where('id',1)->first();
        $conser = Conservancies::where('id',$id)->first();
        return view ('memberwebsite.updateconservancies')->with([
            'conser' => $conser,
            'logo' => $logo,
        ]);
    }

    public function editcons(Request $request)
   {
            $files = $request->validate([
               
                'size' => 'required|numeric',
                'sl' => 'required|numeric',
                'monthly' => 'required|numeric',
                'rmgc' => 'required|numeric',
                'gst' => 'required|numeric',

                
    ]);

     $c = Conservancies::where('id',$request->id)->update([
                    'size' => $request->size,
                    'sl_charge' => $request->sl,
                    'security_charge' => $request->monthly,
                    'rmgc_charge' => $request->rmgc,
                    'gst' => $request->gst,

                ]);

                return redirect()->back()->withSuccess('Great! Conservancies edited successfully.');
       
   }

   public function deletebill($id)
   {
       $d = Conservancies::where('id',$id)->delete();
        return redirect()->back()->withSuccess('Great! Conservancies Deleted successfully.');
   }

   public function addcon(Request $request)
   {
        $files = $request->validate([
               
                'size' => 'required|numeric',
                'sl' => 'required|numeric',
                'security' => 'required|numeric',
                'rmgc' => 'required|numeric',
                'gst' => 'required|numeric',

                
    ]);

    $con = new Conservancies();
    $con->size = $request->size;
    $con->sl_charge = $request->sl;
    $con->security_charge = $request->security;
    $con->rmgc_charge = $request->rmgc;
    $con->gst = $request->gst;

    if ($con->save())
{
    return redirect()->back()->withSuccess('Great! Conservancies Added successfully.');
}
   }
}
