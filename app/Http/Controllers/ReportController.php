<?php
namespace App\Http\Controllers;
use App\UserRecord;
use App\Reports\MyReport;

class ReportController extends Controller
{
    public function __contruct()
    {
        $this->middleware("guest");
    }
    public function index()
    {
        $report = new MyReport;
        $data = UserRecord::all();
        $report->run();
        return view("report",["report"=>$report, 'data'=>$data]);
    }
}