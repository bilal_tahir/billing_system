<?php

namespace App\Http\Controllers;

use App\MemberBillPayment;
use Illuminate\Http\Request;

class MemberBillPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberBillPayment  $memberBillPayment
     * @return \Illuminate\Http\Response
     */
    public function show(MemberBillPayment $memberBillPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberBillPayment  $memberBillPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(MemberBillPayment $memberBillPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberBillPayment  $memberBillPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberBillPayment $memberBillPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberBillPayment  $memberBillPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberBillPayment $memberBillPayment)
    {
        //
    }
}
