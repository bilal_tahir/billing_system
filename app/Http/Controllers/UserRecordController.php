<?php

namespace App\Http\Controllers;

use App\UserRecord;
use App\Water;
use App\Conservancies;
use App\Logo;
use App\Record;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserRecord  $userRecord
     * @return \Illuminate\Http\Response
     */
    public function show(UserRecord $userRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserRecord  $userRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRecord $userRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserRecord  $userRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRecord $userRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserRecord  $userRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRecord $userRecord)
    {
        //
    }

       public function postrecord(Request $request)
    {
         $files = $request->validate([
                'name' => 
                array(
                    'required',
                    'regex:/^[a-zA-Z ]*$/'

                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                'membership' => 'required|min:3',
                'sdo' =>   array(
                    'required',
                    'regex:/^[a-zA-Z ]*$/'

                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                
                'plot' => 'required|integer',
                'size' => 'required',
                'street' => 'required',

                
                'block' =>    array(
                    'required',
                    'regex:/^[A-Z ]*$/',
                    'max:1',

                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                'cnic' =>  array(
                    'required',
                    

                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                'contact1' => array(
                    'required',
                    

                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                'contact2' => array(
                    'required',
                    
                    // if (!preg_match("/^[a-zA-Z ]*$/",$name))


                ),
                'address' => 'required',
                
                // 'paymentmode' => 'required',
                'mutation' => 'required',
                'possession' => 'required',
                'status' => 'required',
                // 'plotprice' => 'required|numeric',
                // 'connection' => 'required|numeric',
                

                
    ]);



    // if($request->status == 'member')
    // {
    //      $files = $request->validate([
    //             'connection' => 'required|numeric',
    //             ]);
    // }
$re = UserRecord::all();
foreach($re as $r)
{
    if($r->membership_no == $request->membership)
    {
        return redirect()->back()->withSuccess('Failed! Regiteration number used previously.');
    }
}
   

        $rec = new UserRecord();
        $rec->name = $request->name;
        $rec->membership_no = $request->membership;
        $rec->sdo = $request->sdo;
        $rec->booking_date = Carbon::now();
        $rec->plot_no = $request->plot;
        $rec->size = $request->size;
        $rec->street_no = $request->street;
        $rec->block = $request->block;
        $rec->cnic = $request->cnic;
        $rec->contact1 = $request->contact1;
        $rec->contact2 = $request->contact2;
        $rec->address = $request->address;
        $rec->email = $request->email;
        // $rec->paymentmode = $request->paymentmode;
        $rec->mutation = $request->mutation;
        $rec->possession = $request->possession;
        $rec->status = $request->status;
        // $rec->no_of_connections = $request->connection;
        // $rec->plotprice = $request->plotprice;
        // $rec->payment_due = $request->plotprice-$request->payment;

        
        
        
        

        if($rec->save())
        {
           

       return redirect()->back()->withSuccess('Great! Registered successfully.');
    
        }
        else{
            return redirect()->back()->withSuccess('Failed! Not Registered successfully.');
        }

    }
     public function enterrecord()
    {
        $logo = Logo::where('id',1)->first();
        $con = Conservancies::all();
        return view ('memberwebsite.record')->with([
            'con' => $con,
            'logo' => $logo,
        ]);
    }

    public function printed(Request $request)
    {
       
        
    }
    public function loadModal($id)
{
    // dd($id);
    // write your process if any
    $rj = UserRecord::where('id',$id)->first();

    $um = Record::where('membership',$rj->membership_no)->first();

    $onemonth = Carbon::now()->subMonth(1)->format('F');
    $twomonth = Carbon::now()->subMonth(2)->format('F');
    $threemonth = Carbon::now()->subMonth(3)->format('F');
    $fourmonth = Carbon::now()->subMonth(4)->format('F');
    $fivemonth = Carbon::now()->subMonth(5)->format('F');
    $sixmonth = Carbon::now()->subMonth(6)->format('F');
    $sevenmonth = Carbon::now()->subMonth(7)->format('F');
    $eightmonth = Carbon::now()->subMonth(8)->format('F');
    $ninemonth = Carbon::now()->subMonth(9)->format('F');
    $tenmonth = Carbon::now()->subMonth(10)->format('F');
    $elevenmonth = Carbon::now()->subMonth(11)->format('F');
    $twelvemonth = Carbon::now()->subMonth(12)->format('F');

    $year = Carbon::now()->year;

    $re = Record::all();

    // dd($rj->membership_no);
    $first = 0;
    $second = 0;
    $three = 0;
    $four = 0;
    $five = 0;
    $six = 0;
    $seven = 0;
    $eight = 0;
    $nine = 0;
    $ten = 0;
    $eleven = 0;
    $twelve = 0;
    foreach($re as $r)
    {
        if( $r->membership == $rj->membership_no && $r->month == 'January' && $r->year == $year )
        {
            $first = $r;
        }
    }

      // second month
      foreach($re as $r)
      {
          if( $r->membership == $rj->membership_no && $r->month == 'February' && $r->year == $year )
          {
              $second = $r;
          }
      }
      
        // three month
        foreach($re as $r)
        {
            if( $r->membership == $rj->membership_no && $r->month == 'March' && $r->year == $year )
            {
                $three = $r;
            }
        }

         // four month
         foreach($re as $r)
         {
             if( $r->membership == $rj->membership_no && $r->month == 'April' && $r->year == $year )
             {
                 $four = $r;
             }
         }
  
         
         // five month
         foreach($re as $r)
         {
             if( $r->membership == $rj->membership_no && $r->month == 'May' && $r->year == $year )
             {
                 $five = $r;
             }
         }

         // six month
         foreach($re as $r)
         {
             if( $r->membership == $rj->membership_no && $r->month == 'June' && $r->year == $year )
             {
                 $six = $r;
             }
         }

          // seven month
          foreach($re as $r)
          {
              if( $r->membership == $rj->membership_no && $r->month == 'July' && $r->year == $year )
              {
                  $seven = $r;
              }
          }

           // eight month
           foreach($re as $r)
           {
               if( $r->membership == $rj->membership_no && $r->month == 'August' && $r->year == $year )
               {
                   $eight = $r;
               }
           }
  
           // nine month
           foreach($re as $r)
           {
               if( $r->membership == $rj->membership_no && $r->month == 'September' && $r->year == $year )
               {
                   $nine = $r;
               }
           }

            // ten month
            foreach($re as $r)
            {
                if( $r->membership == $rj->membership_no && $r->month == 'October' && $r->year == $year )
                {
                    $ten = $r;
                }
            }

             // eleven month
             foreach($re as $r)
             {
                 if( $r->membership == $rj->membership_no && $r->month == 'November' && $r->year == $year )
                 {
                     $eleven = $r;
                 }
             }

             // twelve month
             foreach($re as $r)
             {
                 if( $r->membership == $rj->membership_no && $r->month == 'December' && $r->year == $year )
                 {
                     $twelve = $r;
                 }
             }
  
            //  dd($six);

    // dd('Not Found');

    return response()->json(['first' => $first, 'second' => $second, 'three' => $three, 'four' => $four, 'five' => $five
    , 'six' => $six, 'seven' => $seven, 'eight' => $eight, 'nine' => $nine, 'ten' => $ten
    , 'eleven' => $eleven, 'twelve' => $twelve]);
    
    
    // return UserRecord::where('id',$id)->first();

    

    // return Response::json(array(
    //     'data' => $rj,  
    //   ));
    

    // return view('memberwebsite.home')->with([
    //     'data' => 'name',
    // ]);
}

public function loadUserRecord($id)
{
    return UserRecord::where('id',$id)->first();
}
}
