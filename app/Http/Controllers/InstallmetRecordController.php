<?php

namespace App\Http\Controllers;

use App\InstallmetRecord;
use Illuminate\Http\Request;

class InstallmetRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InstallmetRecord  $installmetRecord
     * @return \Illuminate\Http\Response
     */
    public function show(InstallmetRecord $installmetRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InstallmetRecord  $installmetRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(InstallmetRecord $installmetRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InstallmetRecord  $installmetRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstallmetRecord $installmetRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InstallmetRecord  $installmetRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstallmetRecord $installmetRecord)
    {
        //
    }
}
