<?php

namespace App\Http\Controllers;
use App\UserRecord;
use App\Water;
use App\Surcharge;
use App\Conservancies;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Dompdf\Dompdf;
use App\Stn;
use App\Logo;
use App\MemberBillPayment;
use Datatables;
use App\Bill;
use App\Record;
use App\PaymentRecord;
class WebsiteController extends Controller
{
    //

    public function indexlogin()
    {
        return view ('memberwebsite.indexlogin');
    }

    public function home()
    {
        $logo = Logo::where('id',1)->first();
        $con = Bill::all();
        return view ('mainwebsite.home')->with([
            'logo' => $logo,
            'con' => $con,
        ]);
    }

      public function member()
    {
        $logo = Logo::where('id',1)->first();
        $ar = Surcharge::all();
        $b = Bill::all();

        return view ('memberwebsite.home')->with([
            'logo' => $logo,
            'b' => $b,
            'ar' => $ar,

        ]);
    }

    public function invoice(Request $request)
    {
   $files = $request->validate([

                'duedate' => 'required|numeric',


    ]);

        $surcharge = Surcharge::all();
        $check = UserRecord::where('arrears','<>',0)->get();

        foreach($check as $ch)
        {

            foreach($surcharge as $sur)
       {
            if($ch->size == $sur->size)
            {

UserRecord::where('id',$ch->id)->update([
        'arrears' => $ch->arrears+$sur->surcharge,
    ]);

}
        }
    }



        $user =  UserRecord::where('status','resident')->get();

        $water = Water::all();

        $con = Conservancies::all();

 $bill = Bill::all();


    //    $ctotal = $con->sl_charge+$con->security_charge+$con->rmgc_charge+$con->gst;
       $date = Carbon::today()->format('d-m-Y');
       $due = Carbon::now()->addDays($request->duedate)->format('d-m-Y');
       $datee = Carbon::now();
       $lastmonth = $datee->subMonth()->format('F');

       $onemonth = Carbon::now()->subMonth(1)->format('F');
       $twomonth = Carbon::now()->subMonth(2)->format('F');
       $threemonth = Carbon::now()->subMonth(3)->format('F');
       $fourmonth = Carbon::now()->subMonth(4)->format('F');
       $fivemonth = Carbon::now()->subMonth(5)->format('F');
       $sixmonth = Carbon::now()->subMonth(6)->format('F');
       $sevenmonth = Carbon::now()->subMonth(7)->format('F');
       $eightmonth = Carbon::now()->subMonth(8)->format('F');
       $ninemonth = Carbon::now()->subMonth(9)->format('F');
       $tenmonth = Carbon::now()->subMonth(10)->format('F');
       $elevenmonth = Carbon::now()->subMonth(11)->format('F');
       $twelvemonth = Carbon::now()->subMonth(12)->format('F');


    //    $ye = Carbon::now();

       $oneyear = Carbon::now()->subMonth(1)->format('Y');
       $twoyear = Carbon::now()->subMonth(2)->format('Y');
       $threeyear = Carbon::now()->subMonth(3)->format('Y');
       $fouryear = Carbon::now()->subMonth(4)->format('Y');
       $fiveyear = Carbon::now()->subMonth(5)->format('Y');
       $sixyear = Carbon::now()->subMonth(6)->format('Y');
       $sevenyear = Carbon::now()->subMonth(7)->format('Y');
       $eightyear = Carbon::now()->subMonth(8)->format('Y');
       $nineyear = Carbon::now()->subMonth(9)->format('Y');
       $tenyear = Carbon::now()->subMonth(10)->format('Y');
       $elevenyear = Carbon::now()->subMonth(11)->format('Y');
       $twelveyear = Carbon::now()->subMonth(12)->format('Y');


       $onerec = MemberBillPayment::where('month',$onemonth)->get();
       $tworec = MemberBillPayment::where('month',$twomonth)->get();
       $threerec = MemberBillPayment::where('month',$threemonth)->get();
       $fourrec = MemberBillPayment::where('month',$fourmonth)->get();
       $fiverec = MemberBillPayment::where('month',$fivemonth)->get();
       $sixrec = MemberBillPayment::where('month',$sixmonth)->get();
       $sevenrec = MemberBillPayment::where('month',$sevenmonth)->get();
       $eightrec = MemberBillPayment::where('month',$eightmonth)->get();
       $ninerec = MemberBillPayment::where('month',$ninemonth)->get();
       $tenrec = MemberBillPayment::where('month',$tenmonth)->get();
       $elevenrec = MemberBillPayment::where('month',$elevenmonth)->get();
       $twelverec = MemberBillPayment::where('month',$twelvemonth)->get();


       $year = now()->year;
    //    dd($year);



    //  $conn = UserRecord::max('no_of_connections');
           $water = Water::all();
    //    dd($conn);
        // $connn = Conservancies::find(1);
    //    $ctotal = $con->sl_charge+$con->security_charge+$con->rmgc_charge+$con->gst;
        // $num = (int)$conn->no_of_connections;
// dd($con);

$us = UserRecord::where('status','resident')->get();
// $conserva = Conservancies::where();

// dd($us[0]->arrears);
foreach($us as $use)
{
    foreach ($bill as $su)
    {
        if($su->size == $use->size)
        {
            // dd($su->charges);
            // dd($use->arrears+$su->charges);
            UserRecord::where('id',$use->id)->update(['arrears'=>$use->arrears+$su->charges,
            'billing_month' => Carbon::now()->subMonth(1)->format('F'),
            'billing_year' => Carbon::now()->subMonth(1)->format('Y'),
            // $use->arrears+($wa->monthly_charges*$i)
            // +$cd->sl_charge+$cd->security_charge+$cd->rmgc_charge+$cd->gst
            ]);
        }
    }
}


$rr = UserRecord::where('status','resident')->get();
$abc = count($rr);
for($i=0; $i<$abc ; $i++)
{
    $tr = New Record();
    $tr->membership = $rr[$i]->membership_no;
    $tr->month = Carbon::now()->format('F');
    $tr->year = Carbon::now()->format('Y');
    $tr->bill = $rr[$i]->arrears;
    $tr->payment = 0;
    $tr->save();
}


$trec = Record::all();

// for($i=1; $i<=$conn;$i++)
//         {

//         //    dd($i);

//         foreach($us as $use)
//         {
//         foreach($water as $wa)
//         {
//         if ($use->size == $wa->size)
//         {
//            $cd = Conservancies::where('size',$use->size)->first();


//             UserRecord::where('no_of_connections',$i)->update(['arrears'=>$use->arrears+($wa->monthly_charges*$i)
//             +$cd->sl_charge+$cd->security_charge+$cd->rmgc_charge+$cd->gst
//             ]);


//         }
//     }
// }

//         }


//         $dompdf = new Dompdf();
// $ss=$dompdf->loadHtml('website.invoice',['user' => $user,
//             'water' => $water,
//             'surcharge' => $surcharge,
//             'con' => $con,
//             'ctotal' => $ctotal,
//             'date' => $date,
//             'lastmonth' => $lastmonth,
//             'year' =>$year,
//             'due' => $due,]);
// $dompdf->stream($ss);

    //    dd($con);

    $stn = Stn::where('id',1)->first();
        return view ('memberwebsite.invoice')->with([
            'user' => $user,
            'waterss' => $water,
            'surcharge' => $surcharge,
            // 'conss' => $con,
            // 'ctotal' => $ctotal,
            'date' => $date,
            'lastmonth' => $lastmonth,
            'year' =>$year,
            'due' => $due,
            'stn' => $stn,
            'datee' => $datee,
            'bill' => $bill,
            'onemonth' => $onemonth,
            'twomonth' => $twomonth,
            'threemonth' => $threemonth,
            'fourmonth' => $fourmonth,
            'fivemonth' => $fivemonth,
            'sixmonth' => $sixmonth,
            'sevenmonth' => $sevenmonth,
            'eightmonth' => $eightmonth,
            'ninemonth' => $ninemonth,
            'tenmonth' => $tenmonth,
            'elevenmonth' => $elevenmonth,
            'twelvemonth' => $twelvemonth,
            'oneyear' => $oneyear,
            'twoyear' => $twoyear,
            'threeyear' => $threeyear,
            'fouryear' => $fouryear,
            'fiveyear' => $fiveyear,
            'sixyear' => $sixyear,
            'sevenyear' => $sevenyear,
            'eightyear' => $eightyear,
            'nineyear' => $nineyear,
            'tenyear' => $tenyear,
            'elevenyear' => $elevenyear,
            'twelveyear' => $twelveyear,
            'onerec' => $onerec,
            'tworec' => $tworec,
            'threerec' => $threerec,
            'fourrec' => $fourrec,
            'fiverec' => $fiverec,
            'sixrec' => $sixrec,
            'sevenrec' => $sevenrec,
            'eightrec' => $eightrec,
            'ninerec' => $ninerec,
            'tenrec' => $tenrec,
            'elevenrec' => $elevenrec,
            'twelverec' => $twelverec,
            'trec' => $trec,
            // 'wtotal' => $wtotal
        ]);

    //       $pdf = \App::make('dompdf.wrapper');
    //  $pdf->loadHTML($this->convert_customer_data_to_html());
    //  return $pdf->stream();

    //   $pdf=PDF::loadView('website.invoice',[ 'user' => $user,
    //         'waterss' => $water,
    //         'surcharge' => $surcharge,
    //         'conss' => $con,
    //         // 'ctotal' => $ctotal,
    //         'date' => $date,
    //         'lastmonth' => $lastmonth,
    //         'year' =>$year,
    //         'due' => $due,s])->setPaper('a3','portrait');
    // return $pdf->stream('sales.pdf');

    }


     public function singleinvoice(Request $request)
    {
   $files = $request->validate([

                'duedate' => 'required|numeric',
                'ntn' => 'required',
    ]);



 $bill = Bill::all();

        $user =  UserRecord::where('membership_no',$request->ntn)->first();
        if($user->status == 'resident')
        {
        // $count = count($user);
        // dd($user);
        $water = Water::all();
        $surcharge = Surcharge::all();
        $con = Conservancies::all();
    //    $ctotal = $con->sl_charge+$con->security_charge+$con->rmgc_charge+$con->gst;
       $date = Carbon::today()->format('d-m-Y');
       $due = Carbon::now()->addDays($request->duedate)->format('d-m-Y');;
       $datee = Carbon::now();
       $lastmonth = $datee->subMonth()->format('F');



       $onemonth = Carbon::now()->subMonth(1)->format('F');
       $twomonth = Carbon::now()->subMonth(2)->format('F');
       $threemonth = Carbon::now()->subMonth(3)->format('F');
       $fourmonth = Carbon::now()->subMonth(4)->format('F');
       $fivemonth = Carbon::now()->subMonth(5)->format('F');
       $sixmonth = Carbon::now()->subMonth(6)->format('F');
       $sevenmonth = Carbon::now()->subMonth(7)->format('F');
       $eightmonth = Carbon::now()->subMonth(8)->format('F');
       $ninemonth = Carbon::now()->subMonth(9)->format('F');
       $tenmonth = Carbon::now()->subMonth(10)->format('F');
       $elevenmonth = Carbon::now()->subMonth(11)->format('F');
       $twelvemonth = Carbon::now()->subMonth(12)->format('F');


    //    $ye = Carbon::now();

       $oneyear = Carbon::now()->subMonth(1)->format('Y');
       $twoyear = Carbon::now()->subMonth(2)->format('Y');
       $threeyear = Carbon::now()->subMonth(3)->format('Y');
       $fouryear = Carbon::now()->subMonth(4)->format('Y');
       $fiveyear = Carbon::now()->subMonth(5)->format('Y');
       $sixyear = Carbon::now()->subMonth(6)->format('Y');
       $sevenyear = Carbon::now()->subMonth(7)->format('Y');
       $eightyear = Carbon::now()->subMonth(8)->format('Y');
       $nineyear = Carbon::now()->subMonth(9)->format('Y');
       $tenyear = Carbon::now()->subMonth(10)->format('Y');
       $elevenyear = Carbon::now()->subMonth(11)->format('Y');
       $twelveyear = Carbon::now()->subMonth(12)->format('Y');


       $onerec = MemberBillPayment::where('month',$onemonth)->get();
       $tworec = MemberBillPayment::where('month',$twomonth)->get();
       $threerec = MemberBillPayment::where('month',$threemonth)->get();
       $fourrec = MemberBillPayment::where('month',$fourmonth)->get();
       $fiverec = MemberBillPayment::where('month',$fivemonth)->get();
       $sixrec = MemberBillPayment::where('month',$sixmonth)->get();
       $sevenrec = MemberBillPayment::where('month',$sevenmonth)->get();
       $eightrec = MemberBillPayment::where('month',$eightmonth)->get();
       $ninerec = MemberBillPayment::where('month',$ninemonth)->get();
       $tenrec = MemberBillPayment::where('month',$tenmonth)->get();
       $elevenrec = MemberBillPayment::where('month',$elevenmonth)->get();
       $twelverec = MemberBillPayment::where('month',$twelvemonth)->get();


       $trec = Record::all();

       $year = now()->year;
    //    dd($year);

    $stn = Stn::where('id',1)->first();

        return view ('memberwebsite.singleinvoice')->with([
            'user' => $user,
            'waterss' => $water,
            'surcharge' => $surcharge,
            'conss' => $con,
            // 'ctotal' => $ctotal,
            'date' => $date,
            'lastmonth' => $lastmonth,
            'year' =>$year,
            'due' => $due,
            'stn' => $stn,
            'datee' => $datee,
            'bill' => $bill,
            'onemonth' => $onemonth,
            'twomonth' => $twomonth,
            'threemonth' => $threemonth,
            'fourmonth' => $fourmonth,
            'fivemonth' => $fivemonth,
            'sixmonth' => $sixmonth,
            'sevenmonth' => $sevenmonth,
            'eightmonth' => $eightmonth,
            'ninemonth' => $ninemonth,
            'tenmonth' => $tenmonth,
            'elevenmonth' => $elevenmonth,
            'twelvemonth' => $twelvemonth,
            'oneyear' => $oneyear,
            'twoyear' => $twoyear,
            'threeyear' => $threeyear,
            'fouryear' => $fouryear,
            'fiveyear' => $fiveyear,
            'sixyear' => $sixyear,
            'sevenyear' => $sevenyear,
            'eightyear' => $eightyear,
            'nineyear' => $nineyear,
            'tenyear' => $tenyear,
            'elevenyear' => $elevenyear,
            'twelveyear' => $twelveyear,
            'onerec' => $onerec,
            'tworec' => $tworec,
            'threerec' => $threerec,
            'fourrec' => $fourrec,
            'fiverec' => $fiverec,
            'sixrec' => $sixrec,
            'sevenrec' => $sevenrec,
            'eightrec' => $eightrec,
            'ninerec' => $ninerec,
            'tenrec' => $tenrec,
            'elevenrec' => $elevenrec,
            'twelverec' => $twelverec,
            'trec' => $trec,
            // 'count' => $count,
            // 'wtotal' => $wtotal
        ]);
        }
        else
        {
             return redirect()->back()->withSuccess('Error! Record not found');
        }
    }

   public function submitbill(Request $request)

   {
    $files = $request->validate([
                'ntn'  => 'required',
    ]);


    $amou = UserRecord::where('membership_no',$request->ntn)->first();

if($amou){

//     $bil = new MemberBillPayment();
//     $bil->membership_no = $request->ntn;
//     $bil->month = Carbon::now()->subMonth(0)->format('F');
//     $bil->year = Carbon::now()->subMonth(0)->format('Y');

//     $bil->amount = $amou->arrears;
//     $bil->save();


//     $trr = Record::where('membership',$request->ntn)->latest()->update([
//         'payment' => $amou->arrears,
//     ]);


//      $bill = UserRecord::where('membership_no',$request->ntn)->update([
//         'arrears' => 0,
//     ]);







// return redirect()->back()->withSuccess('Bill Added Successfully');
    }
    else{
      return redirect()->back()->withSuccess('Error! Record not found');
    }

   }


   public function accounts()
   {
       $logo = Logo::where('id',1)->first();

       return view ('memberwebsite.account')->with([
           'logo' => $logo,
       ]);
   }

   public function clearaccounts()
   {
       $logo = Logo::where('id',1)->first();
       $tamou = MemberBillPayment::where('date',Carbon::today()->format('d-m-Y'))->get();
       $tamo = 0;
       foreach($tamou as $tam)
       {
           $tamo = $tamo+$tam->amount;
       }
       $paid = UserRecord::where([
           'arrears'=>0,
           'status'=>'resident',
           ])->get();
    //    dd($paid[0]->membership_no);
return view ('memberwebsite.clearaccount')->with([
           'paid'=>$paid,
           'logo' => $logo,
           'tamo' =>$tamo,

       ]);
   }

   public function nonclearaccounts()
   {
       $logo = Logo::where('id',1)->first();

  $unpaid  = UserRecord::where('arrears','<>',0)->where('status','resident')->get();
  return view ('memberwebsite.nonclearaccount')->with([

           'unpaid' => $unpaid,
           'logo' => $logo,
       ]);
   }

//    public function billsinfo()
//    {
//        $co = Conservancies::all();
//        $wa = Water::all();
//        $ar = Surcharge::all();
//        $stn = Stn::where('id',1)->first();
//        $logo = Logo::where('id',1)->first();

//        return view ('memberwebsite.update')->with([
//             'co' => $co,
//             'wa' => $wa,
//             'ar' => $ar,
//             'stn' => $stn,
//             'logo' => $logo,
//        ]);
//    }

   public function viewbill()
   {



        $surcharge = Surcharge::all();

       $user =  UserRecord::where('status','resident')->get();

       $rec = MemberBillPayment::where('id',1)->first();

       $date = Carbon::today()->format('d-m-Y');
       $datee = Carbon::now();
       $lastmonth = $datee->subMonth(1)->format('F');

       $onemonth = Carbon::now()->subMonth(1)->format('F');
       $twomonth = Carbon::now()->subMonth(2)->format('F');
       $threemonth = Carbon::now()->subMonth(3)->format('F');
       $fourmonth = Carbon::now()->subMonth(4)->format('F');
       $fivemonth = Carbon::now()->subMonth(5)->format('F');
       $sixmonth = Carbon::now()->subMonth(6)->format('F');
       $sevenmonth = Carbon::now()->subMonth(7)->format('F');
       $eightmonth = Carbon::now()->subMonth(8)->format('F');
       $ninemonth = Carbon::now()->subMonth(9)->format('F');
       $tenmonth = Carbon::now()->subMonth(10)->format('F');
       $elevenmonth = Carbon::now()->subMonth(11)->format('F');
       $twelvemonth = Carbon::now()->subMonth(12)->format('F');


    //    $ye = Carbon::now();

       $oneyear = Carbon::now()->subMonth(1)->format('Y');
       $twoyear = Carbon::now()->subMonth(2)->format('Y');
       $threeyear = Carbon::now()->subMonth(3)->format('Y');
       $fouryear = Carbon::now()->subMonth(4)->format('Y');
       $fiveyear = Carbon::now()->subMonth(5)->format('Y');
       $sixyear = Carbon::now()->subMonth(6)->format('Y');
       $sevenyear = Carbon::now()->subMonth(7)->format('Y');
       $eightyear = Carbon::now()->subMonth(8)->format('Y');
       $nineyear = Carbon::now()->subMonth(9)->format('Y');
       $tenyear = Carbon::now()->subMonth(10)->format('Y');
       $elevenyear = Carbon::now()->subMonth(11)->format('Y');
       $twelveyear = Carbon::now()->subMonth(12)->format('Y');


       $onerec = MemberBillPayment::where('month',$onemonth)->get();
       $tworec = MemberBillPayment::where('month',$twomonth)->get();
       $threerec = MemberBillPayment::where('month',$threemonth)->get();
       $fourrec = MemberBillPayment::where('month',$fourmonth)->get();
       $fiverec = MemberBillPayment::where('month',$fivemonth)->get();
       $sixrec = MemberBillPayment::where('month',$sixmonth)->get();
       $sevenrec = MemberBillPayment::where('month',$sevenmonth)->get();
       $eightrec = MemberBillPayment::where('month',$eightmonth)->get();
       $ninerec = MemberBillPayment::where('month',$ninemonth)->get();
       $tenrec = MemberBillPayment::where('month',$tenmonth)->get();
       $elevenrec = MemberBillPayment::where('month',$elevenmonth)->get();
       $twelverec = MemberBillPayment::where('month',$twelvemonth)->get();

    //    dd($onemonth);


    //    dd($lastmonth);
       $year = now()->year;

       $trec = Record::all();


       $bill = Bill::all();


    //    dd($sixyear);


    // dd($onerec);


    $stn = Stn::where('id',1)->first();
        return view ('memberwebsite.viewinvoice')->with([
            'user' => $user,
            'bill' => $bill,
            'surcharge' => $surcharge,
            'date' => $date,
            'lastmonth' => $lastmonth,
            'year' =>$year,
            'datee' => $datee,
            'onemonth' => $onemonth,
            'twomonth' => $twomonth,
            'threemonth' => $threemonth,
            'fourmonth' => $fourmonth,
            'fivemonth' => $fivemonth,
            'sixmonth' => $sixmonth,
            'sevenmonth' => $sevenmonth,
            'eightmonth' => $eightmonth,
            'ninemonth' => $ninemonth,
            'tenmonth' => $tenmonth,
            'elevenmonth' => $elevenmonth,
            'twelvemonth' => $twelvemonth,
            'oneyear' => $oneyear,
            'twoyear' => $twoyear,
            'threeyear' => $threeyear,
            'fouryear' => $fouryear,
            'fiveyear' => $fiveyear,
            'sixyear' => $sixyear,
            'sevenyear' => $sevenyear,
            'eightyear' => $eightyear,
            'nineyear' => $nineyear,
            'tenyear' => $tenyear,
            'elevenyear' => $elevenyear,
            'twelveyear' => $twelveyear,
            'onerec' => $onerec,
            'tworec' => $tworec,
            'threerec' => $threerec,
            'fourrec' => $fourrec,
            'fiverec' => $fiverec,
            'sixrec' => $sixrec,
            'sevenrec' => $sevenrec,
            'eightrec' => $eightrec,
            'ninerec' => $ninerec,
            'tenrec' => $tenrec,
            'elevenrec' => $elevenrec,
            'twelverec' => $twelverec,
            'trec' => $trec,

        ]);


   }

//    public function billsubmit()
//    {
//        $logo = Logo::where('id',1)->first();
//         return view ('memberwebsite.submitbill')->with([
//             'logo' => $logo,
//         ]);
//    }

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rm = UserRecord::where('status','resident');
        return Datatables::of($rm)
        ->addColumn('payment', function($row) {
            return '<a href="/adduserbill/'. $row->id .'" class="btn btn-primary">Submit Bill</a>';
        })
        ->editColumn('undo', function ($row) {
            return '<a href="/undouserbill/'. $row->id .'" class="btn btn-danger">Undo</a>';
        })
        ->addColumn('view', function($row) {
            return '<button data-toggle="modal" data-target="#view-modal" id="getUser"  data-url="/dynamicModal/'. $row->id .'" class="btn btn-success">User Detail</button>';
        })
        ->rawColumns(['undo' => 'undo','payment' => 'payment','view' => 'view'])
        ->make(true);
    }


    public function indexuser()
    {
        $rm = UserRecord::all();
        return Datatables::of($rm)
        ->addColumn('view', function($row) {
            return '<button data-toggle="modal" data-target="#view-modal" id="getUser"  data-url="/dynamicuserdetail/'. $row->id .'" class="btn btn-success">User Detail</button>';
        })



        ->rawColumns(['view' => 'view'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     return view('displaydata');
    // }


}
