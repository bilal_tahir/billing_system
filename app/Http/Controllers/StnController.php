<?php

namespace App\Http\Controllers;

use App\Stn;
use App\Logo;
use Illuminate\Http\Request;

class StnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stn  $stn
     * @return \Illuminate\Http\Response
     */
    public function show(Stn $stn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stn  $stn
     * @return \Illuminate\Http\Response
     */
    public function edit(Stn $stn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stn  $stn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stn $stn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stn  $stn
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stn $stn)
    {
        //
    }
    public function editstn($id)
    {
        $logo = Logo::where('id',1)->first();
        $stn = Stn::where('id',$id)->first();
        return view ('memberwebsite.updatestn')->with([
            'stn'=>$stn,
            'logo' => $logo,
        ]);
          
    }

    public function stnedit(Request $request)
    {

        // dd($request);

         $files = $request->validate([
               
                'ntn' => 'required',
             
                'strn' => 'required',
               

                
    ]);

    
        Stn::where('id',$request->id)->update([
            'ntn' => $request->ntn,
            'strn'  => $request->strn,
        ]);

        return redirect()->back()->withSuccess('Great! Numbers edited successfully.');

    }
}
