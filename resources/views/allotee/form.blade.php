<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Billing System</title>



 <!-- Bootstrap CSS-->
    <link href="{{asset('printasset/bootstrap.min.css')}}" rel="stylesheet" media="all">



    <!-- Main CSS-->
    <link href="{{asset('printasset/theme.css')}}" rel="stylesheet" media="all">

   <style>
    /* body {
        height: 842px;
        width: 595px;
        /* to centre page on screen
        margin-left: auto;
        margin-right: auto;
    } */

.pagebreak {
 page-break-before: always;
}
       </style>

</head>


<body>
  <button class="btn btn-info" onclick="window.print()">Print</button>
  <div class = "container" >
    <div class="pagebreak">
    <div class="row" style="text-align: center;  ">
        <div class="col-12">
          <h3 style="color: black; font-size:46px; color:	#32CD32">NEW AIRPORT TOWN </h3>
            <h4 style="margin-bottom:10px; color:black; font-size:28px;"> A Project of C.S.M Builders & Developers Pvt. Ltd. </h4>
            <h4 style="margin-bottom:10px; color:white; font-size:34px; background-color:#32CD32;
            margin-left: 290px;
    margin-right: 290px;
    border: 1px solid transparent;
    border-radius: 20px;
"> APPLICATION FORM </h4>

        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <h4 style="margin-bottom:10px; color:	#32CD32; font-size:24px;"> <i>Plot Details </i></h4>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-7">
                    <h4 style="margin-bottom:10px; color:	#32CD32; font-size:24px; float: right;"><i> Registeration No </i></h4>
                </div>
                <div class="col-5">
                   <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           {{$alote->registerationNo}}       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                    {{-- <hr style="color: black"> --}}
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <h4 style="margin-bottom:10px; color: black; font-size:20px;"><i> Residential Plots </i></h4>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-7">
                    <h4 style="margin-bottom:10px; color:	#32CD32; font-size:24px; float: right;"> <i>Booking Date</i> </h4>
                </div>
                <div class="col-5">
                   <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          {{$alote->bookingDate}}     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;     </u></h5>
                    {{-- <hr style="color: black"> --}}
                </div>
            </div>

        </div>
    </div>
<div class="row">
    <div class="col-12">
   <div class="cus_form">
       <ul>
           <li>  <h5 style="font-size:18px">25*40 </h5> </li>
       <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:60px">
        @if($alote->plotSize == '25*40')
        <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
        @endif
    </li>
            <li>  <h5 style="font-size:18px">25*50 </h5> </li>
            <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:60px">
                @if($alote->plotSize == '25*50')
                <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
                @endif
            </li>
            <li>  <h5 style="font-size:18px">30*60 </h5> </li>
            <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:60px">
                @if($alote->plotSize == '30*60')
                <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
                @endif
            </li>
            <li>  <h5 style="font-size:18px">35*70 </h5> </li>
            <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:60px">
                @if($alote->plotSize == '35*70')
                <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
                @endif
            </li>
            <li>  <h5 style="font-size:18px">50*90 </h5> </li>
            <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:60px">
                @if($alote->plotSize == '50*90')
                <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
                @endif
            </li>
            <li>  <h5 style="font-size:18px">Other Size </h5>
                @if($alote->plotSize == 'Other' && $alote->plotType =='residential')
                <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:10px" />
                @endif
            </li>
            <li style="border: 2px solid black; border-top:3px solid black; height: 30px; width:80px"></li>
       </ul>
   </div>
  </div>
</div>
<div class="row">
    <div class="col-12">
        <h4 style="margin-bottom:10px; color: black; font-size:20px;"><i> Commercial Plots </i></h4>
    </div>
</div>
<div class="row">
    <div class="col-4">
         <b style="font-size: 18px; color:black; margin-left:15px;">Commercial 5 Marla</b>
        <div style="width: 100px; height:30px; border:2px solid black; float: right; margin-left:10px">
            @if($alote->plotSize == '5 Marla')
             <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:25px" />
             @endif
            </div>
    </div>
    <div class="col-8">
       <b style="float: left; font-size: 18px; color:black ; margin-left:15px"> Other Size</b>
        <div style=" margin-left:20px; width: 100px; height:30px; border:2px solid black; float: left;">
            @if($alote->plotSize == 'Other' && $alote->plotType =='commercial')
            <img src="{{asset('assets/images/check.png')}}" style="height: -webkit-fill-available; margin-left:25px" />
            @endif
           </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 20px">
    <div class="col-4" >
        <div class="row">
            <div class="col-4">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Plot No</h4>
            </div>
            <div class="col-8">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;           {{$alote->plotNo}}   &nbsp;&nbsp;      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="row">
            <div class="col-4">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Street</h4>
            </div>
            <div class="col-8">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;           {{$alote->streetNo}}   &nbsp;&nbsp;      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
    <div class="col-4" >
        <div class="row">
            <div class="col-4">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Block</h4>
            </div>
            <div class="col-8">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      &nbsp;&nbsp;       {{$alote->block}}    &nbsp;&nbsp;     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h4 style="margin-bottom:10px; color: #32CD32;  font-size:20px; margin-top:10px"><i> Personal Data </i></h4>
    </div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-1" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Name </h6>
    </div>
<div class="col-11" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->name}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> S/o, W/o, D/o </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->fName}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> N.I.C No </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black;  ">
    <h5 style=" text-align:center;"> {{$alote->nicNo}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Phone No </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->phoneNo}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Postal Address </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->postalAddress}}  </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-3" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Permanent Address </h6>
    </div>
<div class="col-9" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->permanentAddress}}  </h5>
</div>
</div>

<div class="row" style="margin-top:10px">
    <div class="col-12">
        <h4 style="margin-bottom:10px; color: #32CD32;  font-size:20px; margin-top:10px"><i> Nomination </i></h4>
    </div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-1" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Name </h6>
    </div>
<div class="col-11" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->nominationName}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> S/o, W/o, D/o </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->nominationfName}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Address </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->nominationAddress}} </h5>
</div>
</div>


<div class="row" style="margin-top:10px">
    <div class="col-12">
        <h4 style="margin-bottom:10px; color: #32CD32;  font-size:20px; margin-top:10px"><i> Payment Details </i></h4>
    </div>
</div>

<div class="row">
    <div class="col-12">
   <div class="cus_form">
       <ul>
           <li style="margin-left:0px !important">  <h5 style="font-size:18px; color:black">Payment Mode </h5> </li>
            <li>  <h5 style="font-size:18px">Installments </h5> </li>
            <li style="border: 2px solid black; height: 30px; width:60px; margin-left:0px !important"></li>
            <li>  <h5 style="font-size:18px"> Lump Sum </h5> </li>
            <li style="border: 2px solid black; height: 30px; width:60px; margin-left:0px !important"></li>

       </ul>
   </div>
  </div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-5" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Cross Cheque/ Pay Order / Demand Draft No  </h6>
    </div>
<div class="col-7" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> </h5>
</div>
</div>

<div class="row" style="margin-top: 20px">
    <div class="col-4" >
        <div class="row">
            <div class="col-5">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Receipt No</h4>
            </div>
            <div class="col-7">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      {{$alote->paymentNo}}              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="row">
            <div class="col-4">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Date</h4>
            </div>
            <div class="col-8">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;          {{$alote->bookingDate}}   &nbsp;&nbsp;      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
    <div class="col-4" >
        <div class="row">
            <div class="col-5">
                <h4 style="margin-bottom:10px; color:	black; font-size:20px; ">Amount Rs</h4>
            </div>
            <div class="col-7">
               <h5> <u>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      {{$alote->amount}}         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      </u></h5>
                {{-- <hr style="color: black"> --}}
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-8" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> In Favour of "<b>M/s C.S.M Builders & Developers (Pvt.) Ltd.</b>" </h6>
    </div>
<div class="col-4" style=" margin-top:60px; display: table-cell; border-top: 1px solid black; ">
    <h5 style=" text-align:center;"> Signature of Applicant </h5>
</div>
</div>

<div class="row" style="margin-top:10px">
    <div class="col-12">
        <h4 style="margin-bottom:10px; color: #32CD32;  font-size:20px; margin-top:10px"> For Office Use Only</h4>
    </div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-2" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> Remarks if any </h6>
    </div>
<div class="col-10" style=" margin-top:10px; display: table-cell; border-bottom: 1px solid black; ">
    <h5 style=" text-align:center;"> {{$alote->remarks}} </h5>
</div>
</div>

<div class="row" style="margin-top:8px">
    <div class="col-8" style="margin-top:10px">
        <h6 style="font-size: 18px; color:black"> </h6>
    </div>
<div class="col-4" style=" margin-top:80px; display: table-cell; border-top: 1px solid black; ">
    <h5 style=" text-align:center;"> Company Stamp & Signature </h5>
</div>
</div>

<div class="row" style="text-align: center; margin-top:10px; ">
    <div class="col-12">
        <h3 style="color: black; font-size:20px;">Head Office: Plot # C-1A, Abu Bakar Siddique Road, Block A </h3>
        <h4 style="margin-bottom:10px; color:black; font-size:20px;">New Airport Town, Near New Islamabad Airport Cargo Gate, Rwp   <br> Ph: 0312-9090007, 0311-9559494 </h4>


    </div>
</div>
    </div>
  </div>

</body>

</html>
