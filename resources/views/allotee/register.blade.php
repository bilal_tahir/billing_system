@extends('website_layout.main')
 @section('content')

         <!-- partial -->
         <div class="main-panel">
           <div class="content-wrapper">
             <div class="row">

               <div class="col-12 grid-margin">
                 <div class="card">
                   <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                    </div>
                    @endif
                     <h2 class="card-title" style="font-size: 18px"><b>REGISTER NEW ALLOTE</b></h2>
                     <form class="form-sample" method="POST" action="/alloteregister">
                        @csrf
                       <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Registeration #</label>
                             <div class="col-sm-9">
                               <input type="text" name="registerationNo" class="form-control" placeholder="Enter Registeration No" />
                               @if ($errors->has('registerationNo'))
                               <span class="text-danger">{{ $errors->first('registerationNo') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                         <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Plot Type</label>
                              <div class="col-sm-9">
                                <select name="plotType" class="form-control">
                                  <option value="residential">Residential</option>
                                  <option value="commercial">Commercial</option>
                                </select>
                              </div>
                            </div>
                          </div>
                       </div>
                       <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Size</label>
                             <div class="col-sm-9">
                               <select name="plotSize" class="form-control">
                                <option value="25*40">25*40</option>
                                 <option value="25*50">25*50</option>
                                 <option value="30*50">30*50</option>
                                 <option value="35*70">35*70</option>
                                 <option value="40*70">40*60</option>
                                 <option value="5 Marla">Commercial 5 Marla</option>
                                 <option value="Other">Other Charges</option>
                               </select>
                             </div>
                           </div>
                         </div>
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Plot No</label>
                             <div class="col-sm-9">
                               <input class="form-control" type="number" name="plotNo" placeholder="Enter Plot No" />
                               @if ($errors->has('plotNo'))
                               <span class="text-danger">{{ $errors->first('plotNo') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Street No</label>
                              <div class="col-sm-9">
                                <input type="number" class="form-control" name="streetNo" placeholder="Enter Street No" />
                                @if ($errors->has('streetNo'))
                               <span class="text-danger">{{ $errors->first('streetNo') }}</span>
                              @endif
                              </div>
                            </div>
                          </div>
                         <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Block</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" id="block" name="block" maxlength="1" placeholder="Enter Block No" />
                                  @if ($errors->has('block'))
                               <span class="text-danger">{{ $errors->first('block') }}</span>
                              @endif
                                </div>
                              </div>
                         </div>
                       </div>
                       <p class="card-description" style="font-size: 16px"><b> Personal Details </b></p>
                       <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Name</label>
                             <div class="col-sm-9">
                               <input type="text" id="name" name="name" class="form-control" placeholder="Enter Name" />
                               @if ($errors->has('name'))
                               <span class="text-danger">{{ $errors->first('name') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">S/o, D/o, W/o</label>
                             <div class="col-sm-9">
                               <input type="text" id="sdo" name="fName" class="form-control" placeholder="Enter Father Name"/>
                               @if ($errors->has('fName'))
                               <span class="text-danger">{{ $errors->first('fName') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Nic #</label>
                             <div class="col-sm-9">
                               <input type="text" id="cnic" name="nicNo" class="form-control" data-inputmask="'mask': '99999-9999999-9'"
                               placeholder="Enter CNIC No"/>
                               @if ($errors->has('nicNo'))
                               <span class="text-danger">{{ $errors->first('nicNo') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Phone #</label>
                             <div class="col-sm-9">
                               <input type="text" id="contact1" name="phoneNo" class="form-control" data-inputmask="'mask': '+\\92 999 9999999'"
                               placeholder="Enter Phone No"/>
                               @if ($errors->has('phoneNo'))
                               <span class="text-danger">{{ $errors->first('phoneNo') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Postal Address</label>
                             <div class="col-sm-9">
                               <input type="text" name="postalAddress" class="form-control" placeholder="Enter Postal Address" />
                               @if ($errors->has('postalAddress'))
                               <span class="text-danger">{{ $errors->first('postalAddress') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                        <div class="col-md-6">
                           <div class="form-group row">
                             <label class="col-sm-3 col-form-label">Permanent Address</label>
                             <div class="col-sm-9">
                               <input type="text" name="permanentAddress" class="form-control" placeholder="Enter Permanent Address" />
                               @if ($errors->has('permanentAddress'))
                               <span class="text-danger">{{ $errors->first('permanentAddress') }}</span>
                              @endif
                             </div>
                           </div>
                         </div>
                      </div>
                      <p class="card-description" style="font-size: 16px"><b> Nomination Details </b></p>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                              <input type="text" id="nName" name="nominationName" class="form-control" placeholder="Enter Nominator Name" />
                              @if ($errors->has('nominationName'))
                               <span class="text-danger">{{ $errors->first('nominationName') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">S/o, D/o, W/o</label>
                            <div class="col-sm-9">
                              <input type="text" id="nSdo" name="nominationfName" class="form-control" placeholder="Enter Nominator Father Name" />
                              @if ($errors->has('nominationfName'))
                               <span class="text-danger">{{ $errors->first('nominationfName') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                              <input type="text" name="nominationAddress" class="form-control" placeholder="Enter Nominator Address" />
                              @if ($errors->has('nominationAddress'))
                               <span class="text-danger">{{ $errors->first('nominationAddress') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      <p class="card-description" style="font-size: 16px"><b> Payment Details </b></p>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Payment Mode</label>
                              <div class="col-sm-9">
                                <select name="paymentMode" class="form-control">
                                  <option value="imnstallment">Installments</option>
                                  <option value="sum">Lump Sum</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3  col-form-label">Payment From</label>
                              <div class="col-sm-9">
                                <select name="paymentFrom" class="form-control">
                                  <option value="cheque">Cross Cheque</option>
                                  <option value="payorder">Pay Order</option>
                                  <option value="draft">Demand draft</option>
                                </select>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Cheque/Draft/Pay Order #</label>
                            <div class="col-sm-9">
                              <input type="number" name="no" class="form-control" placeholder="Enter Number of Checque/Draft/Pay Order" />
                              @if ($errors->has('no'))
                               <span class="text-danger">{{ $errors->first('no') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Amount</label>
                            <div class="col-sm-9">
                              <input type="number" name="amount" class="form-control" placeholder="Enter Amount" />
                              @if ($errors->has('amount'))
                               <span class="text-danger">{{ $errors->first('amount') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      <p class="card-description" style="font-size: 16px"><b> Official Details </b></p>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Remarks (if any)</label>
                            <div class="col-sm-9">
                              <input type="text" name="remarks" class="form-control" placeholder="Enter Remarks (officially)" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                     </form>
                   </div>
                 </div>
               </div>
             </div>
           </div>
           <!-- content-wrapper ends -->

           <!-- partial -->
         </div>


 @endsection

 @section('page-level-js')

     {{-- <script>

      $('.cnic').inputmask('(99999-9999999-9');
        </script>    --}}
        <script>
          $(function(){
  $('#name').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(function(){
  $('#nName').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

$(function(){
  $('#sdo').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

$(function(){
  $('#nSdo').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


// $(function(){
//   $('#plot').keypress(function(e){
//     // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
//     let allow_char = [48,49,50,51,52,53,54,55,56,57];
//     if(allow_char.indexOf(e.which) !== -1 ){
//       //do something
//       $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



//     }
//     else{
//       $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
//       return false;

//     }
//   });
// });


// $(function(){
//   $('#street').keypress(function(e){
//     // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
//     let allow_char = [48,49,50,51,52,53,54,55,56,57];
//     if(allow_char.indexOf(e.which) !== -1 ){
//       //do something
//       $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



//     }
//     else{
//       $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
//       return false;

//     }
//   });
// });


$(function(){
  $('#block').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(document).ready(function() {
  $('#cnic').inputmask();
});

$(function(){
  $('#cnic').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

$(document).ready(function() {
  $('#contact1').inputmask();
});

$(function(){
  $('#contact1').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


// $(document).ready(function() {
//   $('#contact2').inputmask();
// });

// $(function(){
//   $('#contact2').keypress(function(e){
//     // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
//     let allow_char = [48,49,50,51,52,53,54,55,56,57];
//     if(allow_char.indexOf(e.which) !== -1 ){
//       //do something
//       $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



//     }
//     else{
//       $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
//       "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
//       return false;

//     }
//   });
// });

        </script>
 @endsection
