@extends('website_layout.main')
 @section('content')

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                  @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>
            @endif


<!-- Modal -->
<div class="modal fade" id="exampleModal0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Move Allote To Member</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/moveallote" method="POST"  enctype="multipart/form-data">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Membership # </label>
     <input type="text" id="membership" name="membership" class="form-control">
            @if ($errors->has('membership'))
                <span class="text-danger">{{ $errors->first('membership') }}</span>
          @endif
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">No of Water Connections </label>
     <input type="text" id="connection" name="connection" class="form-control">
            @if ($errors->has('connection'))
                <span class="text-danger">{{ $errors->first('connection') }}</span>
          @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>


<!-- Modal Giving Possession -->
<div class="modal fade" id="exampleModal09" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Give Allotee Possesion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/possession" method="POST"  enctype="multipart/form-data">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Membership # </label>
     <input type="text" id="membership" name="membership" class="form-control">
            @if ($errors->has('membership'))
                <span class="text-danger">{{ $errors->first('membership') }}</span>
          @endif
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>


<!-- Modal Insert Installment -->
<div class="modal fade" id="exampleModal08" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/addinstallment" method="POST"  enctype="multipart/form-data">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Membership # </label>
     <input type="text" id="membership" name="membership" class="form-control">
            @if ($errors->has('membership'))
                <span class="text-danger">{{ $errors->first('membership') }}</span>
          @endif
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Amount </label>
     <input type="number" id="amount" name="amount" class="form-control">
            @if ($errors->has('amount'))
                <span class="text-danger">{{ $errors->first('amount') }}</span>
          @endif
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Dashboard</li>
                                    </ul>
                                </div>
                                <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                            <span>{{Auth::user()->name}}</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                            <button type="button" class="btn btn-outline-warning"><a href="{{url('/allotelist')}}">
                                    <h2 class="number">Allote List</h2></a>
                                      </button>
                                {{-- <span class="desc">items sold</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--green">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal0">
                                    <h2 class="number">Move Allotee</h2>
                                      </button>
                                {{-- <span class="desc">items sold</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>

                            </div>
                        </div>

                         <div class="col-md-6 col-lg-3">
                            <div class="statistic__item"  style="background-color: black">
                            <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#exampleModal09">
                                    <h2 class="number">Give Allotee <br>Possession</h2>
                            </button>
                                {{-- <span class="desc">items sold</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>

                            </div>
                        </div>

                          {{-- <div class="col-md-6 col-lg-3">
                            <div class="statistic__item"  style="background-color: lightgrey;">
                            <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModal08">
                                    <h2 class="number" style="color:black">Insert <br>Installment</h2>
                            </button>
                                {{-- <span class="desc">items sold</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>

                            </div>
                        </div> --}}

                         <div class="col-md-6 col-lg-3">
                            <div class="statistic__item"  style="background-color: lightgrey;">
                            <a href="/receiptdata" class="btn btn-outline-light">
                                    <h2 class="number" style="color:black">Receive <br>Installment & <br>Print Receipt</h2>
                            </a>
                                {{-- <span class="desc">items sold</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>

                            </div>
                        </div>




                    </div>
                </div>
            </section>
            <!-- END STATISTIC-->



            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                  <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>
 @endsection