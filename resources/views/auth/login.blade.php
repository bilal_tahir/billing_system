<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Star Admin Premium Bootstrap Admin Dashboard Template</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{('assets/vendors/iconfonts/ionicons/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{('assets/vendors/iconfonts/typicons/src/font/typicons.css')}}">
    <link rel="stylesheet" href="{{('assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{('assets/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{('assets/css/shared/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{('assets/images/favicon.png')}}" />
  </head>
  <body>

<!--new -->
  <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auto-form-wrapper" style="opacity: 0.8">
                 <form method="POST" action="{{ route('login') }}" >
                        @csrf
                        <div class="text-block text-center my-3">
                    <span class="text-medium font-weight-semibold"><h2>New AirPort Town</h2></span>
                    <a  class="text-black text-medium"><b>A Project of C.S.M Builders & Developers (PVT) Ltd.</b></a>
                   

                  </div>

                      <div class="form-group">
                            <label for="email"><b>{{ __('E-Mail Address') }}</b></label>

                            <div class="input-group">
                                <input id="email" type="email" class=" form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                      </div>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                



                    <div class="form-group">
                            <label for="password" ><b>{{ __('Password') }}</b></label>

                            <div class="input-group">
                                <input id="password" type="password" class=" form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
 <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                      </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                  <div class="form-group">
                    <button class="btn btn-primary submit-btn btn-block">Login</button>
                  </div>

                         <div class="text-block text-center my-3">
                {{-- <ul class="auth-footer">
                <li>
                  <a href="#" style="color: black">Conditions</a>
                </li>
                <li>
                  <a href="#" style="color: black">Help</a>
                </li>
                <li>
                  <a href="#" style="color: black">Terms</a>
                </li>
              </ul> --}}
              <p class="footer-text text-center" style="color: black">copyright © 2020 New Airport Town. All rights reserved.</p>   

                  </div>
                 
                  
                </form>
              </div>
              
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
<!-- new end -->

    
    
   <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{('assets/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{('assets/js/shared/off-canvas.js')}}"></script>
 
    <!-- endinject -->
  </body>
</html>
