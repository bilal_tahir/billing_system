@extends('website_layout.main')
 @section('content')

 <div id="view-modal" class="modal fade"
    tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true" style="display: none;">
     <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title">User Billing Detail</h3>
            </div>
               <div class="modal-body">
                   <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        {{-- <h2 class="card-title">User Profile</h2> --}}
                        {{-- <p class="card-description"> Add class <code>.table-striped</code> </p> --}}
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th> Month </th>
                              <th> Status </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>January</td>

                              <td id="firststatus"> - </td>
                            </tr>
                            <tr>
                              <td>February</td>

                              <td id="secondstatus"> - </td>
                            </tr>
                            <tr>
                              <td>March</td>

                              <td id="threestatus"> - </td>
                            </tr>
                            <tr>
                              <td>April</td>

                              <td id="fourstatus"> - </td>
                            </tr>
                            <tr>
                              <td>May</td>

                              <td id="fivestatus"> - </td>
                            </tr>
                            <tr>
                              <td>June</td>

                              <td id="sixstatus"> - </td>
                            </tr>
                            <tr>
                              <td>July</td>

                              <td id="sevenstatus"> - </td>
                            </tr>
                            <tr>
                              <td>August</td>

                              <td id="eightstatus"> - </td>
                            </tr>
                            <tr>
                              <td>September</td>

                              <td id="ninestatus"> - </td>
                            </tr>
                            <tr>
                              <td>October</td>

                              <td id="tenstatus"> - </td>
                            </tr>
                            <tr>
                              <td>November</td>

                              <td id="elevenstatus"> - </td>
                            </tr>
                            <tr>
                              <td>December</td>

                              <td id="twelvestatus"> - </td>
                            </tr>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                      <button type="button"
                          class="btn btn-default"
                          data-dismiss="modal">
                          Close
                      </button>
                </div>

         </div>
      </div>
</div><!-- /.modal -->



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enter Due Date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/invoice" method="POST">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Number of Days (From Today)</label>
    <input type="number" name="duedate" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Number Only">
                                                                        @if ($errors->has('duedate'))
                    <span class="text-danger">{{ $errors->first('duedate') }}</span>
                @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Due Date With Membership Number</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form action="/singleinvoice" method="POST">
            @csrf
             <div class="form-group">
    <label for="exampleInputEmail1">Registeration #</label>
    <input type="text" name="ntn" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="registeration number">
                                                                        @if ($errors->has('ntn'))
                    <span class="text-danger">{{ $errors->first('ntn') }}</span>
                @endif
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Number of Days (From Today)</label>
    <input type="number" name="duedate" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Number Only">
                                                                        @if ($errors->has('duedate'))
                    <span class="text-danger">{{ $errors->first('duedate') }}</span>
                @endif
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>




        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                <div class="card">
                  <div class="row">

                    <div class="col-md-6">
                              @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>
            @endif
                      <div class="card-body">
                        <h4 class="card-title">Print Bills</h4>
                        {{-- <p class="card-description">Use any of the available button classes to quickly create a styled button.</p> --}}
                        <div class="template-demo">

                          <button type="button" class="btn btn-dark btn-fw btnc" data-toggle="modal" data-target="#exampleModal">
                            <i class="mdi mdi-printer"></i>Print All Bill</button>
                          <button type="button" class="btn btn-info btn-fw" data-toggle="modal" data-target="#exampleModal2">
                            <i class="mdi mdi-printer"></i>Print Single Bill</button>
                          <a href="{{url('/viewbill')}}" type="button" class="btn btn-success btn-fw">
                            <i class="mdi mdi-printer"></i>View All Bill</a>

                        </div>
                      </div>
                    </div>


                  </div>

                </div>
              </div>




             <div class="col-lg-12 ">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;" >
                  <h4 class="card-title">Pay User Bill</h4>
                  {{-- <p class="card-description"> Add class <code>.table-{color}</code> </p> --}}
                   <table class="table table-bordered table-hover" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Registeration #</th>
                     <th>Plot #</th>
                     <th>Street #</th>
                     <th>Block</th>
                     <th>CNIC</th>
                     <th>Contact</th>
                     <th>Arrears</th>
                     <th>Status</th>
                     <th>Payment</th>
                     <th>Undo</th>
                     <th>View</th>

                  </tr>
               </thead>
            </table>
                </div>
              </div>
            </div>











          </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a  target="_blank">New Airport Town</a>. All rights reserved.</span>
              {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i> --}}
              </span>
            </div>
          </footer>
          <!-- partial -->

        </div>
        <!-- main-panel ends -->


 @endsection

 @section('page-level-js')

     <script>

         $(function() {
               $('#table').DataTable({
               processing: true,
               'language': {
                "processing": "<img style='width:40px; height:40px;' src='assets/images/5.gif' />",
                searchPlaceholder: "Search records",
        } ,
               serverSide: true,
               ajax: '{{ url('index') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'membership_no', name: 'membership_no', orderable: false },
                        { data: 'plot_no', name: 'plot_no' , orderable: false  },
                        { data: 'street_no', name: 'street_no' , orderable: false  },
                        { data: 'block', name: 'block' , orderable: false  },
                        { data: 'cnic', name: 'cnic' , orderable: false },
                        { data: 'contact1', name: 'contact1' , orderable: false  },
                        { data: 'arrears', name: 'arrears' , orderable: false  },
                        { data: 'status', name: 'status' , orderable: false  },
                        {data: 'payment', name: 'payment', orderable: false, searchable: false},
                        {data: 'undo', name: 'undo', orderable: false, searchable: false},
                        {data: 'view', name: 'view', orderable: false, searchable: false}



                     ]
            });

         });
        </script>
        <script>
          $(document).ready(function(){

              $(document).on('click', '#getUser', function(e){

                  e.preventDefault();

                  var url = $(this).data('url');

                  $('#dynamic-content').html(''); // leave it blank before ajax call
                  $('#modal-loader').show();      // load ajax loader

                  $.ajax({
                      url: url,
                      type: 'GET',
                      dataType: 'html'
                  })
                  .done(function(data){
                      console.log(data);

                      var dat = JSON.parse(data);
                      console.log(dat.first.month);

                      console.log(dat.six);

                      $('#dynamic-content').html('');
                      if(dat.first != 0)
                      {
                      $('#firststatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#firststatus').html(' - ');
                      }
                      if(dat.second != 0)
                      {
                      $('#secondstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#secondstatus').html(' - ');
                      }
                      if(dat.three != 0)
                      {
                      $('#threestatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#threestatus').html(' - ');
                      }
                      if(dat.four != 0)
                      {
                      $('#fourstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#fourstatus').html(' - ');
                      }
                      if(dat.five != 0)
                      {
                      $('#fivestatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#fivestatus').html(' - ');
                      }
                      if(dat.six != 0)
                      {
                      $('#sixstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#sixstatus').html(' - ');
                      }
                      if(dat.seven != 0)
                      {
                      $('#sevenstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#sevenstatus').html(' - ');
                      }
                      if(dat.eight != 0)
                      {
                      $('#eightstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#eightstatus').html(' - ');
                      }
                      if(dat.nine != 0)
                      {
                      $('#ninestatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#ninestatus').html(' - ');
                      }
                      if(dat.ten != 0)
                      {
                      $('#tenstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#tenstatus').html(' - ');
                      }
                      if(dat.eleven != 0)
                      {
                      $('#elevenstatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#elevenstatus').html(' - ');
                      }
                      if(dat.twelve != 0)
                      {
                      $('#twelvestatus').html('Bill Paid');
                      }
                      else
                      {
                      $('#twelvestatus').html(' - ');
                      }
                      // load response
                      $('#modal-loader').hide();        // hide ajax loader
                  })
                  .fail(function(){
                      $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                      $('#modal-loader').hide();
                  });

              });

          });

          </script>
 @endsection


            {{-- <div class="container" style="overflow-x:auto;">

               <h2>Members Record</h2>
            <table class="table table-bordered table-hover" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Registeration #</th>
                     <th>Plot #</th>
                     <th>Street #</th>
                     <th>Block</th>
                     <th>Arrears</th>
                  </tr>
               </thead>
            </table>
         </div> --}}