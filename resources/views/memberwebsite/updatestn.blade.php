@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">
                                    @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-10">
                                <div class="card">
                                    <div class="card-header">Billing System</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Edit NTN & STRN NUMBERS <a type="button" href="{{url('/')}}" class="btn btn-dark" style="float: right; color:white">Back To Home</a></h3>
                                        </div>
                                        <hr>
                                    <form action="/stnedit" method="POST" novalidate="novalidate">
@csrf
                                        <input type="hidden" value="{{$stn->id}}" name="id">
                                                     <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="ntn" class="control-label mb-1">NTN #</label>
                                                        <input id="ntn" name="ntn" type="text" class="form-control ntn" value="{{$stn->ntn}}" 
                                                            autocomplete="ntn">
                                                                                        @if ($errors->has('ntn'))
                    <span class="text-danger">{{ $errors->first('ntn') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                                    <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="strn" class="control-label mb-1">STRN #</label>
                                                        <input id="strn" name="strn" type="text" class="form-control strn" value="{{$stn->strn}}" 
                                                            autocomplete="strn">
                                                                                        @if ($errors->has('strn'))
                    <span class="text-danger">{{ $errors->first('strn') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                            </div>

                                         
                                         

                                           

                                         
                                         
                                                
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">Edit</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                          
                         
                        
                            
                          
                           
                           
                          
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                     <p>Copyright © 2020. All rights reserved by <a > New Airport Town</a>.</p>
                                      </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection