@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">
                                    @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-10">
                                <div class="card">
                                <div class="card-header">Billing System <a type="button" href="{{url('/')}}" class="btn btn-dark" style="float: right; color:white">Back To Home</a></div>
                                    
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Register a New Record</h3>
                                        </div>
                                        <hr>
                                        <form action="/postrecord" method="POST" novalidate="novalidate">
@csrf
                                                     <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="name" class="control-label mb-1">Name </label>
                                                        <input id="name" name="name" type="text" class="form-control name" value="" 
                                                            autocomplete="name">
                                                                                        @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                                
                                              
                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="membership" class="control-label mb-1">Membership #</label>
                                                        <input id="membership" name="membership" type="text" class="form-control membership" value="" 
                                                            autocomplete="membership">
                                                                                    @if ($errors->has('membership'))
                    <span class="text-danger">{{ $errors->first('membership') }}</span>
                @endif
                                                    </div>
                                                </div>
                                            </div>
                                          
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="sdo" class="control-label mb-1">S/D of</label>
                                                        <input id="sdo" name="sdo" type="text" class="form-control sdo" value="" 
                                                            autocomplete="sdo">
                                                                                    @if ($errors->has('sdo'))
                    <span class="text-danger">{{ $errors->first('sdo') }}</span>
                @endif
                                                    </div>
                                                </div>
                                                    <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="connection" class="control-label mb-1">Water Connections</label>
                                                        <input id="connection" name="connection" type="number" class="form-control connection" value="" 
                                                            autocomplete="connection">
                                                                                    @if ($errors->has('connection'))
                    <span class="text-danger">{{ $errors->first('connection') }}</span>
                @endif
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="plot" class="control-label mb-1">Plot No</label>
                                                        <input id="plot" name="plot" type="text" class="form-control plot" value="" 
                                                            autocomplete="plot">
                                                                          @if ($errors->has('plot'))
                    <span class="text-danger">{{ $errors->first('plot') }}</span>
                @endif                                               
                                                    </div>
                                                </div>
                                              <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="size" class="control-label mb-1">Size</label>
                                                        <select name="size" id="size" class="form-control">
                                                            @foreach($con as $c)
                                                        <option value="{{$c->size}}">{{$c->size}}</option>
                                                        @endforeach
                                                    </select>
                                                                          @if ($errors->has('size'))
                    <span class="text-danger">{{ $errors->first('size') }}</span>
                @endif                                               
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="street" class="control-label mb-1">Street No</label>
                                                        <input id="street" name="street" type="text" class="form-control street" value=""
                                                            autocomplete="street">
                                                             @if ($errors->has('street'))
                    <span class="text-danger">{{ $errors->first('street') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="block" class="control-label mb-1">Block</label>
                                                        <input id="block" name="block" type="text" class="form-control block" value=""
                                                            autocomplete="block">
                                                             @if ($errors->has('block'))
                    <span class="text-danger">{{ $errors->first('block') }}</span>
                @endif                
                                                    </div>
                                                </div>
                                                 
                                            </div>

                                                       <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="cnic" class="control-label mb-1">CNIC</label>
                                                        <input id="cnic" name="cnic" type="text" class="form-control cnic" value=""
                                                            autocomplete="cnic">
                                                             @if ($errors->has('cnic'))
                    <span class="text-danger">{{ $errors->first('cnic') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="contact1" class="control-label mb-1">Contact 1</label>
                                                        <input id="contact1" name="contact1" type="text" class="form-control contact1" value=""
                                                            autocomplete="contact1">
                                                             @if ($errors->has('contact1'))
                    <span class="text-danger">{{ $errors->first('contact1') }}</span>
                @endif                
                                                    </div>
                                                </div>
                                                 
                                            </div>

                                                       <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="contact2" class="control-label mb-1">Contact 2</label>
                                                        <input id="contact2" name="contact2" type="text" class="form-control contact2" value=""
                                                            autocomplete="contact2">
                                                             @if ($errors->has('contact2'))
                    <span class="text-danger">{{ $errors->first('contact2') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="address" class="control-label mb-1">Address</label>
                                                        <input id="address" name="address" type="text" class="form-control address" value=""
                                                            autocomplete="address">
                                                             @if ($errors->has('address'))
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                @endif                
                                                    </div>
                                                </div>
                                                 
                                            </div>

                                                       <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="email" class="control-label mb-1">Email</label>
                                                        <input id="email" name="email" type="email" class="form-control email" value=""
                                                            autocomplete="email">
                                                             @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="paymentmode" class="control-label mb-1">Payment Mode</label>
                                                        <select id="paymentmode" name="paymentmode" class="form-control">
                                                        <option value="cash">Cash</option>
                                                        <option value="installment">Installment</option>
                                                        
                                                        </select>
                                                             @if ($errors->has('paymentmode'))
                    <span class="text-danger">{{ $errors->first('paymentmode') }}</span>
                @endif                
                                                    </div>
                                                </div>
                                                 
                                            </div>

                                            
                                                       <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="mutation" class="control-label mb-1">Mutation</label><br>
                                                       <select id="mutation" name="mutation" class="form-control">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                        
                                                        </select>
                                                             @if ($errors->has('mutation'))
                    <span class="text-danger">{{ $errors->first('mutation') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                 <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="possession" class="control-label mb-1">Possession</label>
                                                       <select id="possession" name="possession" class="form-control">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                        
                                                        </select>
                                                             @if ($errors->has('possession'))
                    <span class="text-danger">{{ $errors->first('possession') }}</span>
                @endif                
                                                    </div>
                                                </div>
                                                 
                                            </div>

                                                    <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="status" class="control-label mb-1">Status</label><br>
                                                       <select id="status" name="status" class="form-control">
                                                        <option value="member">Member</option>
                                                        <option value="allote">Allote</option>
                                                        
                                                        </select>
                                                             @if ($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                                                    <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="plotprice" class="control-label mb-1">Plot Price</label><br>
                                                        <input id="plotprice" name="plotprice" type="number" class="form-control plotprice" value=""
                                                            autocomplete="plotprice">
                                                             @if ($errors->has('plotprice'))
                    <span class="text-danger">{{ $errors->first('plotprice') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                 
                                                 
                                            </div>


                                                   <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="payment" class="control-label mb-1">Payment (if any)</label><br>
                                                       <input id="payment" name="payment" type="number" class="form-control payment" value="0"
                                                            autocomplete="payment">
                                                             @if ($errors->has('payment'))
                    <span class="text-danger">{{ $errors->first('payment') }}</span>
                @endif                
                                                    </div>
                                                </div>

                                          

                                 
                                                 
                                            </div>
                                         
                                                
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">Register</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                          
                         
                        
                            
                          
                           
                           
                          
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                     <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                     </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection