<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Billing System</title>

 <!-- Bootstrap CSS-->
    <link href="{{asset('printasset/bootstrap.min.css')}}" rel="stylesheet" media="all">



    <!-- Main CSS-->
    <link href="{{asset('printasset/theme.css')}}" rel="stylesheet" media="all">

   <style>
    /* body {
        height: 842px;
        width: 595px;
        /* to centre page on screen
        margin-left: auto;
        margin-right: auto;
    } */
 
.pagebreak {
 page-break-before: always;
}
       </style>

</head>


<body>
     <button class="btn btn-info" onclick="window.print()">Print</button>
<h2 style="text-align: center; margin-top: 100px;  font-size:80px">{{$datee}}</h2>
    <div class="pagebreak">

<div class = "container" >
    @if(isset($user))
    <div class="row" style="text-align: center; border: 2px solid black; border-bottom:1px solid black; ">
        <div class="col-12">
          <h3 style="color: black; font-size:26px;">NEW AIRPORT TOWN </h3>
            <h4 style="margin-bottom:10px; color:black;  font-size:24px;"> A Project of C.S.M Builders & Developers Pvt. Ltd.  <br> MAINTENANCE BILL </h4>
    
        </div>
    </div>
<div class="row" style="text-align: center; border:2px solid black; border-top:1px solid black; border-bottom ">
    <div class="col-4">
        <h4 style="border-bottom:2px solid black">BILLING MONTH</h4>
 <h4 style="margin-bottom: 10px;"> {{$lastmonth}} - {{$year}} </h4>
    </div>
    <div class="col-4" style="border-left: 2px solid black;">
        <h4 style="border-bottom:2px solid black">ISSUE DATE</h4>
 <h4 style="margin-bottom: 10px;"> {{$date}} </h4>
    </div>
    <div class="col-4" style="border-left: 2px solid black;">
        <h4 style="border-bottom:2px solid black">DUE DATE</h4>
 <h4 style="margin-bottom: 10px;"> {{$due}} </h4>
    </div>
</div>

<div class="row">
    <div class="col-7" style="border: 2px solid black;">
        <div style="float:left;  margin-top:10px; margin-bottom:10px; width:100% ">
        <table style="border: 2px solid black; width: -webkit-fill-available;">
  
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">Registration No</th>
        <th style="border: 2px solid black; padding:5px; color:black;  padding-left:50px; ">{{$user->membership_no}}</th>
        
    </tr>
   
   
     

</table>
    </div>
        <div style="margin-top: 20px; ">
                <h3 style="font-size:26px; color:black;">{{$user->name}}</h3>
 <h5>PLOT/HOUSE NO {{$user->plot_no}}, STREET NO {{$user->street_no}}, BLOCK {{$user->block}}</h5>
        </div>
          <div style="margin-top: 20px">
               <div style="float:left;  margin-top:10px; margin-bottom:10px; width:100% ">
        <table style="border: 2px solid black; width: -webkit-fill-available;">
        
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">Maint Charges</th>
        <?php $r =0; ?>
        @foreach($bill as $bils)
        @if($user->size == $bils->size)
    <td style="border: 2px solid black; padding:5px; color:black; ">{{$bils->charges}}</td>
    <?php 
$r = $bils->charges;
    ?>
    @endif
        @endforeach
        <th style="border: 2px solid black; padding:5px; color:black;">Misc Charges</th>
        <td style="border: 2px solid black; padding:5px; color:black; ">0</td>
    </tr>
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">Violation Charges</th>
        <td style="border: 2px solid black; padding:5px; color:black; ">0</td>
        <th style="border: 2px solid black; padding:5px; color:black;">Advance Payment</th>
        <td style="border: 2px solid black; padding:5px; color:black; ">0</td>
    </tr>
    <tr>
      <th style="border: 2px solid black; padding:5px; color:black;">Installment Arrears</th>
      <td style="border: 2px solid black; padding:5px; color:black; ">0</td>
    
  </tr>
   
     

</table>
    </div>
        </div>
 

         <div style="margin-top: 20px; ">
                <h3 style="margin-bottom: 10px">Instructions</h3>
 <h5 style="margin-bottom: 10px">Monthly Maintenance charges are against Services being provided like, Street Lights, Garbage Collection, Security Guards, Road/Street Cleaning,
     Park/Green Belt Maintenance, Vacant Plots Cleaning, Masjid Maintenance, Sewerage/Drain opening external. </h5>
       
    <table style="border: 2px solid black; width: -webkit-fill-available; margin-left:0x; margin-right:0px">
  <?php
  $a = 0;
  ?>
@foreach($bill as $bills)
  
        <tr>
        <th style="border: 2px solid black; padding:5px; color:black;"> <?php
$a = $a+1;
echo $a.')';
   ?></th>
        
        <th style="border-bottom:2px solid black; padding:5px; color:black;">{{$bills->marla}}</th>
        <td style="border-bottom:2px solid black; padding:5px; color:black; ">Rs {{$bills->charges}}/-</td>
    </tr>
    
    @endforeach
  
     

      

</table>

<h3 style="margin-bottom: 10px; margin-top:10px; color:black">Contact No's</h3>
<h5 style="margin-bottom: 10px"><ul><li style="list-style: none;margin-bottom:5px; margin-left: 10px; font-size:22px"> 1) 0312-9090007 </li><li style="list-style: none;margin-bottom:5px; margin-left: 10px; font-size:22px"> 2) 0311-9559494</li></ul></h5>
    
<h3 style="margin-bottom: 10px; margin-top:10px; color:black; font-size:20px">Note: This bill is only payable in  C.S.M Builders office located at:<br>
    Plot No C-1A, Abu-Bakar Siddique Road, Block A, <br> New Airport Town, Rawalpindi.</h3>
    
    </div>
 

    </div>
    <div class="col-5" style="border: 2px solid black;">
              <div style="float:left;  margin-top:10px; margin-bottom:10px; width:100%; border-bottom: 2px solid black ">
        <table style="width: -webkit-fill-available;">
            
          <tr>
            <th style="border: 2px solid black; padding:5px; color:black; text-align:center">Month</th>
            <th style="border: 2px solid black; padding:5px; color:black; text-align:center ">Year</th>
            <th style="border: 2px solid black; padding:5px; color:black; text-align:center">Amount</th>
            <th style="border: 2px solid black; padding:5px; color:black; text-align:center ">Status</th>
        </tr>
     
         <tr>
         <td style=" padding:5px; color:black; text-align:center">{{$onemonth}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$oneyear}}</td>
         {{-- @foreach($onerec as $or)
         @if(($or->month == $onemonth) && ($or->year == $oneyear) && ($or->membership_no==$user->membership_no))
         <td style=" padding:5px; color:black; text-align:center;">{{$or->amount}}</td>
         <td style=" padding:5px; color:black; text-align:center;"> @if(isset($or->amount))
        0
        @else
        <h3>Not Paid</h3>
        @endif
        </td></td>
         @else
         <td style=" padding:5px; color:black; text-align:center;"><h4>Not Paid</h4></td>
         <td style=" padding:5px; color:black; text-align:center;"><h4>Not Paid</h4></td>
         @endif
          @endforeach --}}
          @foreach($trec as $tr)
          @if(($onemonth == $tr->month) && ($oneyear == $tr->year) && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
       
        </tr>
    
    
        <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$twomonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$twoyear}}</td>
          @foreach($trec as $tr)
          @if(($twomonth == $tr->month) && ($twoyear == $tr->year)  &&  ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$threemonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$threeyear}}</td>
          @foreach($trec as $tr)
          @if(($threemonth == $tr->month) && ($threeyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$fourmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$fouryear}}</td>
          @foreach($trec as $tr)
          @if(($fourmonth == $tr->month) && ($fouryear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$fivemonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$fiveyear}}</td>
          @foreach($trec as $tr)
          @if(($fivemonth == $tr->month) && ($fiveyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$sixmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$sixyear}}</td>
          @foreach($trec as $tr)
          @if(($sixmonth == $tr->month) && ($sixyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$sevenmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$sevenyear}}</td>
          @foreach($trec as $tr)
          @if(($sevenmonth == $tr->month) && ($sevenyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$eightmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$eightyear}}</td>
          @foreach($trec as $tr)
          @if(($eightmonth == $tr->month) && ($eightyear == $tr->year) && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$ninemonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$nineyear}}</td>
          @foreach($trec as $tr)
          @if(($ninemonth == $tr->month) && ($nineyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$tenmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$tenyear}}</td>
          @foreach($trec as $tr)
          @if(($tenmonth == $tr->month) && ($tenyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
    
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$elevenmonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$elevenyear}}</td>
          @foreach($trec as $tr)
          @if(($elevenmonth == $tr->month) && ($elevenyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
         <tr>
          <td style=" padding:5px; color:black; text-align:center">{{$twelvemonth}}</td>
          <td style=" padding:5px; color:black; text-align:center;">{{$twelveyear}}</td>
          @foreach($trec as $tr)
          @if(($twelvemonth == $tr->month) && ($twelveyear == $tr->year)  && ($tr->membership == $user->membership_no))
          <td style=" padding:5px; color:black; text-align:center">{{$tr->bill}}</td>
         <td style=" padding:5px; color:black; text-align:center;">{{$tr->payment}}</td>
         @endif
    
          @endforeach
        
         </tr>
       
</table>
    </div>


         <div style="float:left;  margin-top:10px; margin-bottom:10px; width:100% ">
        <table style="border: 2px solid black; width: -webkit-fill-available;">
        
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">CURRENT BILL</th>
        <?php
        $bi = 0;
        ?>
        @foreach($bill as $b)
        @if($user->size == $b->size)
    <td style="border: 2px solid black; padding:5px; color:black; ">{{$b->charges}}</td>
    <?php
    $bi = $b->charges;
?>
@endif
        @endforeach
    </tr>
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">ARREARS/AGE</th>
    <td style="border: 2px solid black; padding:5px; color:black; ">{{$user->arrears}}</td>
    
    </tr>
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">TOTAL</th>
    <td style="border: 2px solid black; padding:5px; color:black; ">{{$bi+$user->arrears}}</td>
       
    </tr>
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">PAYMENT WITHIN DUE DATE</th>
        <td style="border: 2px solid black; padding:5px; color:black; ">{{$bi+$user->arrears}}</td>
    </tr>
    <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">LP. SURCHARGE</th>
        <?php
        $su = 0;
        ?>
@foreach($surcharge as $sur)
        @if($user->size == $sur->size)
    <td style="border: 2px solid black; padding:5px; color:black; ">{{$sur->surcharge}}</td>
    <?php
    $su = $sur->surcharge;
    ?>
@endif
        @endforeach
    </tr>
      <tr>
        <th style="border: 2px solid black; padding:5px; color:black;">PAYMENT AFTER DUE DATE</th>
        <td style="border: 2px solid black; padding:5px; color:black; ">{{$bi+$user->arrears+$su}}</td>
    </tr>
     

</table>
    </div>

    
    </div>
</div>


<div class="row" style="text-align: center; border: 2px solid black; border-bottom:1px solid black">
    <div class="col-12">
        <h3 style="padding: 60px; text-align:center;">Adds Section</h3>
 {{-- <h5> Head Office: Plot # C-1A, Abu Bakar Siddique Road, Block A <br> New Airport Town, Near New Islamabad Airport Cargo Gate, Rwp. </h5> --}}

    </div>
</div>
<div class="row">
    <div class="col-5">
        <hr style="border-top:2px dotted black;">
    </div>
    <div class="col-2" style="text-align:center; color: black">
    {{-- <img src="{{asset('/asset/images/scissors.png')}}" style="height: 25px"/> --}}
     Cut Here
    </div>
    <div class="col-5">
        <hr style="border-top:2px dotted black;">
    </div>
</div>

<div class="row" style="text-align: center; border: 2px solid black; border-bottom:1px solid black;">
    <div class="col-12">
        <h3 style="color: black; font-size:26px;">NEW AIRPORT TOWN  <sub style="float: right; margin-top:10px;">Office Copy</sub></h3>
      <h4 style="margin-bottom:10px; color:black; font-size:24px;"> A Project of C.S.M Builders & Developers Pvt. Ltd.  <br> MAINTENANCE BILL </h4>

        <div style="margin-top: 20px; text-align:left !important; ">
         <h3 style="color: black; font-size:24px;">{{$user->name}}</h3>
       <h5 style="color:black">PLOT/HOUSE NO {{$user->plot_no}}, STREET NO {{$user->street_no}}, BLOCK {{$user->block}}</h5>
       </div>
 {{-- <h5> Head Office: Plot # C-1A, Abu Bakar Siddique Road, Block A <br> New Airport Town, Near New Islamabad Airport Cargo Gate, Rwp. </h5> --}}
     <div style="float:left;  margin-top:10px; margin-bottom:10px; width:100% ">
        <table style="border: 2px solid black; width: -webkit-fill-available;">
{{--         
            <tr>
                <th style="border: 2px solid black; padding:5px; color:black;">Name: </th>
            <th style="borher: 2px solid black; padding:5px; color:black;" colspan="2">{{$user->name}}</td>
                <th style="border: 2px solid black; padding:5px; color:black;">Maint Charges</th>
            <th style="border: 2px solid black; padding:5px; color:black;">{{$r}}</th>
               
            </tr> --}}
            <tr>
              <th style="border: 2px solid black; padding:5px; color:black;">Bill Month</th>
              <th style="border: 2px solid black; padding:5px; color:black; ">Due Date</th>
              <th style="border: 2px solid black; padding:5px; color:black;">Registration  No</th>
              <th style="border: 2px solid black; padding:5px; color:black; ">PAYMENT WITHIN DUE DATE</th>
              <td style="border: 2px solid black; padding:5px; color:black; ">{{$bi+$user->arrears}}</td>
          </tr>
          <tr>
              <td style="border: 2px solid black; padding:5px; color:black;">{{$lastmonth}} {{$year}}</td>
              <td style="border: 2px solid black; padding:5px; color:black; ">26-06-2020</td>
          <td style="border: 2px solid black; padding:5px; color:black;">{{$user->membership_no}}</td>
              <th style="border: 2px solid black; padding:5px; color:black; ">PAYMENT AFTER DUE DATE</th>
              <td style="border: 2px solid black; padding:5px; color:black; ">{{$bi+$user->arrears+$su}}</td>
          </tr>
     

</table>
    </div>
    </div>
</div>

@else
<h3>Sorry:)) No Record Found !!</h3>
@endif
</div>



    </div>

  

</body>

</html>
<!-- end document-->