@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">
                                    @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-10">
                                <div class="card">
                                <div class="card-header">Billing System <a type="button" href="{{url('/')}}" class="btn btn-dark" style="float: right; color:white">Back To Home</a></div>
                                    
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Register a New Record</h3>
                                        </div>
                                        <hr>
                                            <form action="/submitbill" method="POST">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Membership # </label>
    <input type="text" name="ntn" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter number ..">
            @if ($errors->has('ntn'))
                <span class="text-danger">{{ $errors->first('ntn') }}</span>
          @endif    
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
                                    </div>
                                </div>
                            </div>

                          
                         
                        
                            
                          
                           
                           
                          
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                     <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                     </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection