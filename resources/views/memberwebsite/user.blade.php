@extends('website_layout.main')
 @section('content')


 <div id="view-modal" class="modal fade"
 tabindex="-1" role="dialog"
 aria-labelledby="myModalLabel"
 aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <h3 class="modal-title">User Profile</h3>
         </div>
            <div class="modal-body">

                {{-- <div id="modal-loader"
                     style="display: none; text-align: center;">
                 <img src="ajax-loader.gif">
                </div> --}}

                <!-- content will be load here -->
                {{-- <div id="dynamic-content"></div> --}}
                <div class="col-lg-12 grid-margin stretch-card">
                 <div class="card">
                   <div class="card-body">
                     {{-- <h2 class="card-title">User Profile</h2> --}}
                     {{-- <p class="card-description"> Add class <code>.table-striped</code> </p> --}}
                     <table class="table table-striped">
                       {{-- <thead>
                         <tr>
                           <th> User </th>
                           <th> First name </th>
                           <th> Progress </th>
                           <th> Amount </th>
                           <th> Deadline </th>
                         </tr>
                       </thead> --}}
                       <tbody>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-1.png" alt="image" /> </td> --}}
                           <td> Name </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           {{-- <td> $ 77.99 </td> --}}
                           <td id="mname"> May 15, 2015 </td>
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-2.png" alt="image" /> </td> --}}
                           <td> Regsiteration # </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-danger" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           {{-- <td> $245.30 </td> --}}
                           <td id="mreg"> July 1, 2015 </td>
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-3.png" alt="image" /> </td> --}}
                           <td> Plot # </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-warning" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           {{-- <td> $138.00 </td> --}}
                           <td id="mplot"> Apr 12, 2015 </td>
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-4.png" alt="image" /> </td> --}}
                           <td> Street # </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           {{-- <td> $ 77.99 </td> --}}
                           <td id="mstreet"> May 15, 2015 </td>
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-1.png" alt="image" /> </td> --}}
                           <td> Block </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-danger" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           <td id="mblock"> $ 160.25 </td>
                           {{-- <td> May 03, 2015 </td> --}}
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-1.png" alt="image" /> </td> --}}
                           <td> CNIC </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-danger" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           <td id="mcnic"> $ 160.25 </td>
                           {{-- <td> May 03, 2015 </td> --}}
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-1.png" alt="image" /> </td> --}}
                           <td> Contact </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-danger" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           <td id="mcontact"> $ 160.25 </td>
                           {{-- <td> May 03, 2015 </td> --}}
                         </tr>
                         <tr>
                           {{-- <td class="py-1">
                             <img src="../../../assets/images/faces-clipart/pic-1.png" alt="image" /> </td> --}}
                           <td> Arrears </td>
                           <td>
                             {{-- <div class="progress">
                               <div class="progress-bar bg-danger" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                             </div> --}}
                           </td>
                           <td id="marrears"> $ 160.25 </td>
                           {{-- <td> May 03, 2015 </td> --}}
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
               </div>

             </div>
             <div class="modal-footer">
                   <button type="button"
                       class="btn btn-default"
                       data-dismiss="modal">
                       Close
                   </button>
             </div>

      </div>
   </div>
</div><!-- /.modal -->



        <!--new-->



        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">





             <div class="col-lg-12 ">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;" >
                  <h4 class="card-title">User Record</h4>
                  {{-- <p class="card-description"> Add class <code>.table-{color}</code> </p> --}}
                   <table class="table table-bordered table-hover" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Registeration #</th>
                     <th>Plot #</th>
                     <th>Street #</th>
                     <th>Block</th>
                     <th>Arrears</th>
                     <th>Status</th>
                     <th>View</th>

                  </tr>
               </thead>
            </table>
                </div>
              </div>
            </div>












          </div>

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a  target="_blank">New Airport Town</a>. All rights reserved.</span>
              {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i> --}}
              </span>
            </div>
          </footer>
          <!-- partial -->

        </div>
        <!-- main-panel ends -->


 @endsection

 @section('page-level-js')

     <script>

         $(function() {
               $('#table').DataTable({
                processing: true,
               'language': {
                "processing": "<img style='width:40px; height:40px;' src='assets/images/5.gif' />",
                searchPlaceholder: "Search records",
        } ,
               serverSide: true,
               ajax: '{{ url('indexuser') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'membership_no', name: 'membership_no'  , orderable: false},
                        { data: 'plot_no', name: 'plot_no'  , orderable: false },
                        { data: 'street_no', name: 'street_no'   , orderable: false},
                        { data: 'block', name: 'block'   , orderable: false},
                        { data: 'arrears', name: 'arrears'  , orderable: false},
                        { data: 'status', name: 'status'   , orderable: false},
                        {data: 'view', name: 'view', orderable: false, searchable: false},



                     ]
            });
         });
        </script>
         <script>
          $(document).ready(function(){

              $(document).on('click', '#getUser', function(e){

                  e.preventDefault();

                  var url = $(this).data('url');

                  $('#dynamic-content').html(''); // leave it blank before ajax call
                  $('#modal-loader').show();      // load ajax loader

                  $.ajax({
                      url: url,
                      type: 'GET',
                      dataType: 'html'
                  })
                  .done(function(data){
                      console.log(data);
                      var dat = JSON.parse(data);
                      $('#dynamic-content').html('');
                      $('#mname').html(dat.name);
                      $('#mreg').html(dat.membership_no);
                      $('#mplot').html(dat.plot_no);
                      $('#mstreet').html(dat.street_no);
                      $('#mblock').html(dat.block);
                      $('#mcnic').html(dat.cnic);
                      $('#mcontact').html(dat.contact1);
                      $('#marrears').html(dat.arrears);
                      // load response
                      $('#modal-loader').hide();        // hide ajax loader
                  })
                  .fail(function(){
                      $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                      $('#modal-loader').hide();
                  });

              });

          });

          </script>
 @endsection

