@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
              <button class="btn btn-info" onclick="window.print()">Print</button>
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">
                        <div class="row">

<?php
$total = 0;
?>
                          
                         <div class="col-12">
                             <h3>Clear Accounts</h3>
                            
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Membership #</th>
                                                <th>plot #</th>
                                                <th>Street #</th>
                                                <th>Block</th>
                                                <th>Arrears</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($paid as $paid)
                                            <tr>
                                            <td>{{$paid->name}}</td>
                                            <td>{{$paid->membership_no}}</td>
                                            <td>{{$paid->plot_no}}</td>
                                            <td>{{$paid->street_no}}</td>
                                             <td>{{$paid->block}}</td>
                                            <td>{{$paid->arrears}}</td>
                                            <?php
                                            $total = $total+$paid->arrears;
                                            ?>
                                            @endforeach
                                            </tr>
                                            
                                          
                                        </tbody>
                                    </table>
                                    <h2>Total Amount: {{$tamo}}</h2>
                                </div>
                            </div>
                           
                          
                        </div>

                       

                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                  <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection