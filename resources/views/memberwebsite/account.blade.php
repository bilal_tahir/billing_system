@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">

                           <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--green">
                           <button class="btn btn-outline-success"> <a href="{{url('/clearaccounts')}}">  <h2 class="number">Clear</h2></a></button>
                                {{-- <span class="desc">members online</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-account-o"></i>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                            <button class="btn btn-outline-success"> <a href="{{url('/nonclearaccounts')}}"> 
                                    <h2 class="number">Non Clear</h2>
                                      </button>
                                {{-- <span class="desc">items sold</span> --}}
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                           
                            </div>
                        </div>
                    </div></div></section>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection