@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container">
                                    @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
                        <div class="row">
                            <div class="col-10">
                                <div class="card">
                                    <div class="card-header">Billing System</div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">Edit Conservancies <a type="button" href="{{url('/')}}" class="btn btn-dark" style="float: right; color:white">Back To Home</a></h3>
                                        </div>
                                        <hr>
                                        <form action="/wateredit" method="POST" novalidate="novalidate">
@csrf

                                        <input type="hidden" value="{{$water->id}}" name="id">
                                                     <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="size" class="control-label mb-1">Size</label>
                                                        <input id="size" name="size" type="number" class="form-control size" value="{{$water->size}}" 
                                                            autocomplete="size">
                                                                                        @if ($errors->has('size'))
                    <span class="text-danger">{{ $errors->first('size') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                                    <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="monthly" class="control-label mb-1">Monthly Charge</label>
                                                        <input id="monthly" name="monthly" type="number" class="form-control monthly" value="{{$water->monthly_charges}}" 
                                                            autocomplete="monthly">
                                                                                        @if ($errors->has('monthly'))
                    <span class="text-danger">{{ $errors->first('monthly') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                            </div>

                                         
                                         

                                           

                                         
                                         
                                                
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">Register</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                          
                         
                        
                            
                          
                           
                           
                          
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2020 Colorlib. All rights reserved by <a href="https://weglobetech.com"> WeGlobe Technologies</a>.</p>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection