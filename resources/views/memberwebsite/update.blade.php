@extends('website_layout.main')
 @section('content')

    
        {{-- <div class="page-content--bgf7">
         
            <section class="au-breadcrumb2">
                <div class="container"> --}}
           


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enter Due Date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/invoice" method="POST">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Number of Days (From Today)</label>
    <input type="number" name="duedate" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Number Only">
                                                                        @if ($errors->has('duedate'))
                    <span class="text-danger">{{ $errors->first('duedate') }}</span>
                @endif    
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>
      
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Due Date With Membership Number</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form action="/singleinvoice" method="POST">
            @csrf
             <div class="form-group">
    <label for="exampleInputEmail1">Registeration #</label>
    <input type="text" name="ntn" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="registeration number">
                                                                        @if ($errors->has('ntn'))
                    <span class="text-danger">{{ $errors->first('ntn') }}</span>
                @endif    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Number of Days (From Today)</label>
    <input type="number" name="duedate" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Number Only">
                                                                        @if ($errors->has('duedate'))
                    <span class="text-danger">{{ $errors->first('duedate') }}</span>
                @endif    
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>
      
    </div>
  </div>
</div>

{{--

                </div>
            </section>
          
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                            <span>{{Auth::user()->name}}</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            

            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                             <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModal">
                                    <h2 class="number">Print All Bill</h2>
                                      </button>
                                      <button type="button" class="btn btn-outline-success btn-fw">
                            <i class="mdi mdi-printer"></i>Print</button>
                                
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                           
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--blue">
                               <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal2"> <h2 class="number">Single Bill</h2></button>
                                <div class="icon">
                                    <i class="zmdi zmdi-calendar-note"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--red">
                            <a href="{{url('/accounts')}}" class="btn btn-outline-danger">  <h2 class="number">Accounts</h2></a>
                               <div class="icon">
                                    <i class="zmdi zmdi-money"></i>
                                </div>
                            </div>
                        </div>
                          <div class="col-md-6 col-lg-3">
                            <div class="statistic__item" style="background-color: lightgrey">
                            <a href="{{url('/billsubmit')}}" class="btn btn-outline-light" >   <h2 class="number" style="color: black">Submit Bill</h2></a>
                               <div class="icon">
                                    <i class="zmdi zmdi-money"></i>
                                </div>
                            </div>
                        </div>
                          <div class="col-md-6 col-lg-3">
                            <div class="statistic__item " style="background-color: black">
                                <a href="{{url('/billsinfo')}}" class="btn btn-outline-dark"> 
                                <h2 class="number">Update Bills</h2>
                                </a>
                              <div class="icon">
                                    <i class="zmdi zmdi-money"></i>
                                </div>
                            </div>
                        </div>

                           <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                            <a href="{{url('/viewbill')}}" class="btn btn-outline-warning">
                                    <h2 class="number">View All Bill</h2>
                             </a>
                                {{-- <span class="desc">items sold</span> 
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                           
                            </div>
                        </div>

                     
                    </div>
                </div>
            </section>

        </div> --}}



        <!--new-->


        
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
               
           
              
         

             <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;">
                  <h4 class="card-title">Update Bill</h4>
                  {{-- <a href="{{url('/viewbill')}}" style="float: right" type="button" class="btn btn-success btn-fw">
                    View All Bill</a> --}}
                  {{-- <p class="card-description"> Add class <code>.table</code> </p> --}}
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Marla </th>
                        <th>Size </th>
                        <th>Charges</th>
                        <th>Update</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($b as $bill)
                      <tr>
                      <td>{{$bill->marla}}</td>
                      <td>{{$bill->size}}</td>
                      <td>{{$bill->charges}}</td>
                      <td><a href="{{url('editbill/'.$bill->id)}}" class="btn btn-info">Edit</a></td>
                      <td><a href="{{url('deletebill/'.$bill->id)}}" class="btn btn-danger">Delete</a></td>
                       
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;">
                  <h4 class="card-title">Update Surcharges</h4>
                  {{-- <p class="card-description"> Add class <code>.table-hover</code> </p> --}}
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Size</th>
                        <th>Surcharge</th>
                        <th>Update</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($ar as $sur)
                      <tr>
                      <td>{{$sur->size}}</td>
                      <td>{{$sur->surcharge}}</td>
                      <td><a href="{{url('editsurcharge/'.$sur->id)}}" class="btn btn-info">Edit</a></td>
                      <td><a href="{{url('deletesurcharge/'.$sur->id)}}" class="btn btn-danger">Delete</a></td>
                      </tr>
                      @endforeach
                     
                    </tbody>
                    
                  </table>
                </div>
              </div>
            </div>

            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;">
                  <h4 class="card-title">Add  New Bill</h4>
                  {{-- <a href="{{url('/viewbill')}}" style="float: right" type="button" class="btn btn-success btn-fw">
                    View All Bill</a> --}}
                  {{-- <p class="card-description"> Add class <code>.table</code> </p> --}}
                  <form class="form-sample" action="/addbill" method="POST" novalidate="novalidate">
                    @csrf
                  <p class="card-description">            @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>
        @endif</p>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Marla</label>
                        <div class="col-sm-9">
                         <input id="marla" name="marla" type="text" class="form-control name" value="" 
                         autocomplete="marla">
                        @if ($errors->has('marla'))
                <span class="text-danger">{{ $errors->first('marla') }}</span>
            @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Size</label>
                        <div class="col-sm-9">
                         <input id="size" name="size" type="number" class="form-control name" value="" 
                         autocomplete="size">
                        @if ($errors->has('size'))
                <span class="text-danger">{{ $errors->first('size') }}</span>
            @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Charges</label>
                        <div class="col-sm-9">
                         <input id="charges" name="charges" type="number" class="form-control name" value="" 
                         autocomplete="charges">
                        @if ($errors->has('charges'))
                <span class="text-danger">{{ $errors->first('charges') }}</span>
            @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success mr-2">Submit</button>
                  </form>



                </div>
              </div>
              
            </div>

            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="overflow-x: auto;">
                  <h4 class="card-title">Add  New Surcharge</h4>
                  {{-- <a href="{{url('/viewbill')}}" style="float: right" type="button" class="btn btn-success btn-fw">
                    View All Bill</a> --}}
                  {{-- <p class="card-description"> Add class <code>.table</code> </p> --}}
                  <form class="form-sample" action="/addsurcharge" method="POST" novalidate="novalidate">
                    @csrf
                  <p class="card-description">            @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>
        @endif</p>
                 

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Size</label>
                        <div class="col-sm-9">
                         <input id="size" name="size" type="number" class="form-control name" value="" 
                         autocomplete="size">
                        @if ($errors->has('size'))
                <span class="text-danger">{{ $errors->first('size') }}</span>
            @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Charges</label>
                        <div class="col-sm-9">
                         <input id="surcharge" name="surcharge" type="number" class="form-control name" value="" 
                         autocomplete="surcharge">
                        @if ($errors->has('surcharge'))
                <span class="text-danger">{{ $errors->first('surcharge') }}</span>
            @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success mr-2">Submit</button>
                  </form>



                </div>
              </div>
              
            </div>
           
            

          



          </div>
         
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a  target="_blank">New Airport Town</a>. All rights reserved.</span>
              {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i> --}}
              </span>
            </div>
          </footer>
          <!-- partial -->
       
        </div>
        <!-- main-panel ends -->
  
        
 @endsection


         