@extends('website_layout.main')
 @section('content')


            <!-- MAIN CONTENT-->
         <div class="main-panel">
        <div class="content-wrapper">
                                   
                        <div class="row">
                            <div class="col-10">
                                <div class="card">
                                    <div class="card">
                                    <div class="card-body">
                                         <h4 class="card-title">Update Bill</h4>
                                        <form action="/billupdated" method="POST" novalidate="novalidate">
                                             @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
@csrf

                                        <input type="hidden" value="{{$sur->id}}" name="id">
                                                     <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="size" class="control-label mb-1">Size</label>
                                                        <input id="size" name="size" type="number" class="form-control size" value="{{$sur->size}}" 
                                                            autocomplete="size">
                                                                                        @if ($errors->has('size'))
                    <span class="text-danger">{{ $errors->first('size') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                                    <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="charges" class="control-label mb-1">charges</label>
                                                        <input id="charges" name="charges" type="number" class="form-control charges" value="{{$sur->charges}}" 
                                                            autocomplete="charges">
                                                                                        @if ($errors->has('charges'))
                    <span class="text-danger">{{ $errors->first('charges') }}</span>
                @endif
                                                       
                                                    </div>
                                                </div>
                                            </div>

                                         
                                         

                                           

                                         
                                         
                                                
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">Edit</span>
                                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                          
                         
                        
                            
                          
                           
                           
                          
                        </div>
                        
                        </div>
                    </div>
                     <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a
                href="http://www.bootstrapdash.com/" target="_blank">New Airport Town</a>. All rights reserved.</span>
            {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i
                class="mdi mdi-heart text-danger"></i>
            </span> --}}
          </div>
        </footer>
                </div>
        
 @endsection