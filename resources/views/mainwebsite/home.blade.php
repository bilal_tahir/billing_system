@extends('website_layout.main')
 @section('content')




            <!-- Modal -->
<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Logo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/updatelogo" method="POST"  enctype="multipart/form-data">
            @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Update Logo </label>
     <input type="file" id="image" name="image" class="form-control-file">
            @if ($errors->has('image'))
                <span class="text-danger">{{ $errors->first('image') }}</span>
          @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>

    </div>
  </div>
</div>

            <!-- STATISTIC-->
            <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Register New Rrecord Member/Allote</h4>
                    <form class="form-sample" action="/postrecord" method="POST" novalidate="novalidate">
                        @csrf
                      <p class="card-description">
                        @if($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>
            @endif</p>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                             <input id="name" name="name" type="text" class="form-control name" value="{{ old('name') }} plac"
                             autocomplete="name" placeholder="Enter Name"  >
                            @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Registeration #</label>
                            <div class="col-sm-9">
                         <input id="membership" name="membership" type="text" class="form-control membership" value="{{ old('membership') }}"
                              autocomplete="membership" placeholder="Enter Resisteratio No">
                       @if ($errors->has('membership'))
                    <span class="text-danger">{{ $errors->first('membership') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">S/D of</label>
                            <div class="col-sm-9">
                           <input id="sdo" name="sdo" type="text" class="form-control sdo" value="{{ old('sdo') }}"
                            autocomplete="sdo" placeholder="Enter Father Name">
                        @if ($errors->has('sdo'))
                    <span class="text-danger">{{ $errors->first('sdo') }}</span>
                @endif
                            </div>
                          </div>
                        </div>

                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Plot No</label>
                            <div class="col-sm-9">
                              <input id="plot" name="plot" type="text" class="form-control plot" value="{{ old('plot') }}"
                                                            autocomplete="plot" placeholder="Enter Plot No">
                                                                          @if ($errors->has('plot'))
                    <span class="text-danger">{{ $errors->first('plot') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Size</label>
                            <div class="col-sm-9">
                                                                 <select name="size" id="size" class="form-control">
                                                            @foreach($con as $c)
                                                        <option value="{{$c->size}}">{{$c->size}}</option>
                                                        @endforeach
                                                    </select>
                                                                          @if ($errors->has('size'))
                    <span class="text-danger">{{ $errors->first('size') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Street No</label>
                            <div class="col-sm-9">
                              <input id="street" name="street" type="text" class="form-control street" value="{{ old('street') }}"
                                                            autocomplete="street" placeholder="Enter Street No">
                                                             @if ($errors->has('street'))
                    <span class="text-danger">{{ $errors->first('street') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Block</label>
                            <div class="col-sm-9">
                              <input id="block" name="block" maxlength="1" type="text" class="form-control block" value="{{ old('block') }}"
                                                            autocomplete="block" placeholder="Enter Block No">
                                                             @if ($errors->has('block'))
                    <span class="text-danger">{{ $errors->first('block') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">CNIC </label>
                            <div class="col-sm-9">
                              <input id="cnic" name="cnic" type="text" class="form-control cnic" value="{{ old('cnic') }}" data-inputmask="'mask': '99999-9999999-9'"
                                                            autocomplete="cnic" placeholder="Enter CNIC Number">
                                                             @if ($errors->has('cnic'))
                    <span class="text-danger">{{ $errors->first('cnic') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Contact 1</label>
                            <div class="col-sm-9">
                             <input id="contact1" name="contact1" type="text" class="form-control contact1"
                                                            autocomplete="contact1" data-inputmask="'mask': '+\\92 999 9999999'" placeholder="Enter Client First Number">
                                                             @if ($errors->has('contact1'))
                    <span class="text-danger">{{ $errors->first('contact1') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Contact 2</label>
                            <div class="col-sm-9">
                              <input id="contact2" name="contact2" type="text" class="form-control contact2" value="{{ old('contact2') }}"
                                                            autocomplete="contact2" data-inputmask="'mask': '+\\92 999 9999999'" placeholder="Enter Client Second Number">
                                                             @if ($errors->has('contact2'))
                    <span class="text-danger">{{ $errors->first('contact2') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                              <input id="address" name="address" type="text" class="form-control address" value="{{ old('address') }}"
                                                            autocomplete="address" placeholder="Enter Address">
                                                             @if ($errors->has('address'))
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                              <input id="email" name="email" type="email" class="form-control email" value="{{ old('email') }}"
                                                            autocomplete="email" placeholder="Enter Email">
                                                             @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                               <select id="status" name="status" class="form-control">
                                                        {{-- <option value="member">Member</option> --}}
                                                        <option value="resident">Resident</option>

                                                        </select>
                                                             @if ($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        {{-- <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Payment Mode</label>
                            <div class="col-sm-9">
                              <select id="paymentmode" name="paymentmode" class="form-control">
                                                        <option value="cash">Cash</option>
                                                        <option value="installment">Installment</option>

                                                        </select>
                                                             @if ($errors->has('paymentmode'))
                    <span class="text-danger">{{ $errors->first('paymentmode') }}</span>
                @endif
                            </div>
                          </div>
                        </div> --}}
                      </div>

                         <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mutation</label>
                            <div class="col-sm-9">
                              <select id="mutation" name="mutation" class="form-control">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>

                                                        </select>
                                                             @if ($errors->has('mutation'))
                    <span class="text-danger">{{ $errors->first('mutation') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Possesion</label>
                            <div class="col-sm-9">
                               <select id="possession" name="possession" class="form-control">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>

                                                        </select>
                                                             @if ($errors->has('possession'))
                    <span class="text-danger">{{ $errors->first('possession') }}</span>
                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                        <div class="row">
                      </div>

                 <button type="submit" class="btn btn-success mr-2">Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a  target="_blank">New Airport Town</a>. All rights reserved.</span>
              {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i> --}}
              </span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
            <!-- END COPYRIGHT-->



 @endsection

 @section('page-level-js')

     {{-- <script>

      $('.cnic').inputmask('(99999-9999999-9');
        </script>    --}}
        <script>
          $(function(){
  $('#name').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

$(function(){
  $('#sdo').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(function(){
  $('#plot').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(function(){
  $('#street').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(function(){
  $('#block').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,
    100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,114,115,116,117,118,119,120,121,122,32];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(document).ready(function() {
  $('#cnic').inputmask();
});

$(function(){
  $('#cnic').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

$(document).ready(function() {
  $('#contact1').inputmask();
});

$(function(){
  $('#contact1').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});


$(document).ready(function() {
  $('#contact2').inputmask();
});

$(function(){
  $('#contact2').keypress(function(e){
    // allowed char: 1 , 2 , 3, 4, 5, N, O, A, B, C
    let allow_char = [48,49,50,51,52,53,54,55,56,57];
    if(allow_char.indexOf(e.which) !== -1 ){
      //do something
      $(".form-control:focus").css({"border-color":"#66afe9" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)" });



    }
    else{
      $(".form-control:focus").css({"border-color":"red" ,  "-webkit-box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)",
      "box-shadow" : "inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(243,175,233,.6)" });
      return false;

    }
  });
});

        </script>
 @endsection
