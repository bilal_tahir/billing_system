<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Print Allote Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/printalloteform" method="POST">
          @csrf
<div class="form-group">
  <label for="exampleInputEmail1">Registeration No</label>
  <input type="text" name="registerationNo" class="form-control">
     @if ($errors->has('registerationNo'))
        <span class="text-danger">{{ $errors->first('registerationNo') }}</span>
      @endif
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row" style="margin-bottom: 0px !important">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="{{url('/')}}">
           New Airport Town</a>
          <a class="navbar-brand brand-logo-mini" href="{{url('/')}}">
          <img src=" {{asset('assets/images/log.png')}}" /> </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
          <ul class="navbar-nav">

          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
            </li>
            <li class="nav-item dropdown">
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>

       <div class="container-fluid page-body-wrapper" style="padding-left: 1px !important">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
           <a class="nav-link " id="UserDropdown" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" data-toggle="dropdown" aria-expanded="false">
               Logout </a>



                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
          </li>
          <li class="nav-item nav-category">Main Menu</li>
          <li class="nav-item">
          <a class="nav-link" href="{{url('/')}}">
              <i class="menu-icon typcn typcn-document-text"></i>
              <span class="menu-title">Users List</span>
            </a>
          </li>



          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon typcn typcn-coffee"></i>
              <span class="menu-title">Billing Section</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/member')}}">Print Bill</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/userrecords')}}">Register New Member</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/updatecon">Update Content</a>
                </li>
              </ul>
            </div>
          </li>


          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-allote" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon typcn typcn-coffee"></i>
              <span class="menu-title">Allotee Section</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-allote">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('/reg/allote')}}">Register Allote</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="modal" data-target="#exampleModal">Print Allote Form</a>
                  {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                  </button> --}}
                </li>
                {{-- <li class="nav-item">
                  <a class="nav-link" href="/updatecon">Update Content</a>
                </li> --}}
              </ul>
            </div>
          </li>


        </ul>
      </nav>