<!doctype html>
<html lang="en">

@include('website_layout.head')


<body>
  <div class="container-scroller">


    @include('website_layout.header')


    @yield('content')



    </div>
</div>

    {{-- @include('website_layout.footer') --}}
     <!-- plugins:js -->
    <script src="{{('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{('assets/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{('assets/js/shared/off-canvas.js')}}"></script>

    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{('assets/js/demo_1/dashboard.js')}}"></script>
    <!-- End custom js for this page-->
 
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
    @yield('page-level-js')
</body>

</html>
