<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/template', function () {
    return view('template');
});
Route::get('/test', function () {
    return view('test');
});

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/','WebsiteController@indexlogin');

Route::get('/foo', function () {
Artisan::call('storage:link');
});

Route::group(['middleware'=>'auth'], function () {

Route::get('/','RecordController@userrec');



Route::get('/userrecords','WebsiteController@home');
Route::get('/updatecon','BillController@updatecon');

Route::get('/member','WebsiteController@member');
Route::post('/invoice','WebsiteController@invoice');
Route::post('/singleinvoice','WebsiteController@singleinvoice');
Route::get('/enterrecord' , 'UserRecordController@enterrecord');
Route::post('/postrecord','UserRecordController@postrecord');
Route::get('/report', "ReportController@index");
Route::post('/printed','UserRecordController@printed');
Route::post('/submitbill','WebsiteController@submitbill');
Route::get('/accounts','WebsiteController@accounts');
Route::get('/clearaccounts','WebsiteController@clearaccounts');
Route::get('/nonclearaccounts','WebsiteController@nonclearaccounts');
// Route::get('/billsinfo','WebsiteController@billsinfo');
Route::get('editconservancies/{id}','ConservanciesController@editbill');
Route::get('deleteconservancies/{id}','ConservanciesController@deletebill');
Route::post('editcons','ConservanciesController@editcons');
Route::get('editwater/{id}','WaterController@editwater');
Route::get('deletewater/{id}','WaterController@deletewater');
Route::post('wateredit','WaterController@wateredit');
Route::get('editsurcharge/{id}','SurchargeController@editsurcharge')->name('edit');
Route::get('deletesurcharge/{id}','SurchargeController@deletesurcharge');
Route::post('editsur','SurchargeController@editsur');
Route::get('editstn/{id}','StnController@editstn');
Route::post('stnedit','StnController@stnedit');
Route::post('/addconservancies', 'ConservanciesController@addcon');
Route::post('/addwater', 'WaterController@addwater');
Route::post('/addsurcharge', 'SurchargeController@addsurcharge');
Route::get('/viewbill','WebsiteController@viewbill');
Route::post('/updatelogo','LogoController@updatelogo');
// Route::get('/billsubmit','WebsiteController@billsubmit');
Route::post('/addbill','BillController@add');
// Route::post('/addsurcharge','BillController@add');

Route::get('/editbill/{id}', 'BillController@updatebill');
Route::get('/deletebill/{id}','BillController@deletebill');
Route::post('/billupdated','BillController@billupdate');

// Route::get('create', 'WebsiteController@create');
Route::get('index', 'WebsiteController@index');

Route::get('indexuser', 'WebsiteController@indexuser');


//PAyment Record Routes
Route::get('/adduserbill/{id}','PaymentRecordController@adduserbill');
Route::get('/undouserbill/{id}','PaymentRecordController@undouserbill');


Route::get('dynamicModal/{id}',[
    'as'=>'dynamicModal',
    'uses'=> 'UserRecordController@loadModal'
]);

Route::get('dynamicuserdetail/{id}',[
    'as'=>'dynamicModal',
    'uses'=> 'UserRecordController@loadUserRecord'
]);





//Atorne Routes
// Route::get('/allote','AlloteController@home');
// Route::get('/allotelist','AlloteController@allotelist');
// Route::post('/moveallote', 'AlloteController@moveallote');
// Route::post('/allotereceipt','AlloteController@receipt');
// Route::post('/possession','AlloteController@possession');
// Route::post('/addinstallment','AlloteController@installment');
// Route::get('/receiptdata','AlloteController@receiptdata');
// Route::post('/addbill','BillController@add');
Route::get('/alloteform','AlloteController@regformview');
Route::get('/reg/allote','AlloteController@regallote');
Route::post('/alloteregister', 'AlloteController@allotereg');
Route::post('/printalloteform','AlloteController@printalloteform');




Route::get('/dynamic_pdf', 'DynamicPDFController@index');
Route::get('/dynamic_pdf/pdf', 'DynamicPDFController@pdf');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



// Route::get('/test/{id}','UserRecordController@loadModal');